// ------------- LOCAL DEPENDENCIES -------------- //

import { Db } from './firebase';

// Constants.

import * as PlatformIDs from '../constants/PlatformIDs';
import * as DatabaseFields from '../constants/DatabaseFields';

// ------------- MAIN -------------- //

/**
 * Creates user document in Firebase Firestore.
 *
 * @param {String} id -> User's UID.
 * @param {String} email -> User's email.
 *
 * @return {Promise} -> Request promise.
 */
export const doCreateUser = (id, email) =>
  Db.collection('users')
  .doc(`${id}`)
  .set({
    [DatabaseFields.EMAIL]      : email,
    [DatabaseFields.FIRST_LOGIN]: (new Date()).toISOString(),
  });

/**
 * Updates user's facebook data.
 *
 * @param {String} id -> User's UID.
 * @param {JSON} userObject -> Updated facebook user object.
 *
 * @return {Promise} -> Request promise.
 */
export const doUpdateFacebookUser = (id, userObject) =>
  Db.collection('users')
  .doc(`${id}`)
  .update({
    [`${PlatformIDs.FACEBOOK}.${DatabaseFields.USER_NAME}`]: userObject.name,
    [`${PlatformIDs.FACEBOOK}.${DatabaseFields.ID}`]: userObject.userID,
    [`${PlatformIDs.FACEBOOK}.${DatabaseFields.ACCESS_TOKEN}`]: userObject.accessToken,
  });

/**
 * Fetches user data from Firebase database.
 *
 * @param {String} id -> User's database uid.
 *
 * @return {Promise} -> Request promise.
 */
export const doFetchUser = (id) =>
  Db.collection('users')
  .doc(`${id}`)
  .get();

/**
 * Updates user's selected Facebook ad account ID in database.
 *
 * @param {String} id -> User's database uid.
 * @param {String} adAccountID -> User's Facebook selected ad account ID.
 *
 * @return {Promise} -> Request promise.
 */
export const doSetSelectedFacebookAdAccountID = (id, adAccountID) =>
  Db.collection('users')
  .doc(`${id}`)
  .update({
    [`${PlatformIDs.FACEBOOK}.${DatabaseFields.SELECTED_AD_ACCOUNT_ID}`]: adAccountID,
  });

/**
 * Fetches FAQ data from database. Data is passed to promise.
 *
 * @return {Promise<Array<Object>>} -> FAQ data array. Null if nonexistent.
 */
export const doFetchFaq = () =>
  Db.collection('faq').get().then((snapshot) => {
    if (!snapshot.empty) {
      const parsedData = [];

      for (let i = 0; i < snapshot.docs.length; i++) {
        parsedData.push(snapshot.docs[i].data());
      }

      return parsedData;
    } else {
      return null;
    }
  });

/**
 * Sets user's last login time in database with ISO date string.
 *
 * @param {String} uid -> User's database uid.
 *
 * @returns {Promise} -> Request promise.
 */
export const doSetLastLogin = (uid) =>
  Db.collection('users')
  .doc(`${uid}`)
  .update({
    [DatabaseFields.LAST_LOGIN]: (new Date()).toISOString(),
  });