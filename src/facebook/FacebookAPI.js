// ------------- DEPENDENCIES ------------ //

const FacebookSDK = require('fbgraph');

// ------------- MAIN ------------ //

/**
 * Initializes Facebook API with user access token
 * & app secret.
 *
 * @param {String} accessToken -> Facebook user access token.
 * @return {JSON} -> Initialized Facebook API object.
 */
export const initAPI = (accessToken) => {
  FacebookSDK.setAccessToken(accessToken);
  FacebookSDK.setAppSecret(process.env.REACT_APP_FACEBOOK_APP_SECRET);
  FacebookSDK.setVersion(process.env.REACT_APP_FACEBOOK_API_VERSION);
  return FacebookSDK;
}

export const fetchAdAccounts = (sdk, userID) =>
  new Promise((resolve, reject) => sdk.get(`${userID}/adaccounts`, (err, resp) => {
    if (err) {
      reject(err);
    } else {
      resolve(resp);
    }
  }));

const createCampaign = (sdk, adAccountID, params) =>
  new Promise((resolve, reject) => sdk.post(`/act_${adAccountID}/campaigns`, {
    objective: 'LEAD_GENERATION',
    status   : 'PAUSED',
    adlabels : [{ name: 'adcentric' }],
    ...params,
  }, (err, resp) => {
    if (err) {
      reject(err);
    } else {
      resolve(resp);
    }
  }));

export const launchCampaign = (sdk, adAccountID, params) =>
  createCampaign(sdk, adAccountID, params);