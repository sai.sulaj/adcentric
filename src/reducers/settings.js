// ------------- LOCAL DEPENDENCIES -------------- //

import * as Actions from '../constants/ActionTypes';

// ------------- CONSTANTS & GLOBAL VARS -------------- //

const INITIAL_STATE = {
  defaultPage: null,
};

// ------------- MAIN -------------- //

const applySetDefaultPage = (_State, _Action) => ({
  ..._State,
  defaultPage: _Action.defaultPage,
});

function settingsReducer(_State = INITIAL_STATE, _Action) {
  switch (_Action.type) {
    case Actions.SETTINGS_DEFAULT_PAGE_SET: {
      return applySetDefaultPage(_State, _Action);
    }
    default: 
      return _State;
  }
}

export default settingsReducer;
