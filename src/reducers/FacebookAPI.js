// ------------- LOCAL DEPENDENCIES -------------- //

import * as FacebookAPI from '../facebook/FacebookAPI';

// Constants.

import * as Actions from '../constants/ActionTypes';

// ------------- CONSTANTS & GLOBAL VARS -------------- //

const INITIAL_STATE = {
  api                : null,
  available          : false,
  selectedAdAccountID: null,
};

// ------------- MAIN -------------- //

const applySetFacebookAPI = (state, action) => ({
  ...state,
  api                 : action.api !== undefined ? action.api : null,
  available           : action.api === null || action.api === undefined ? false : true,
});

const applyUpdateFacebookAPIAccessToken = (state, action) => ({
  ...state,
  api: FacebookAPI.initAPI(action.newAccessToken),
});

const applyUnsetFacebookAPI = (state) => ({
  ...state,
  api            : null,
  available      : false,
});

const applyUnsetFacebookAPISelectedAccountID = (state, action) => ({
  ...state,
  selectedAdAccountID : action.selectedAdAccountID === null || action.selectedAdAccountID === undefined ? null : action.selectedAdAccountID,
});

function facebookAPIReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Actions.FACEBOOK_API_SET: {
      return applySetFacebookAPI(state, action);
    }
    case Actions.FACEBOOK_API_ACCESS_TOKEN_UPDATE: {
      return applyUpdateFacebookAPIAccessToken(state, action);
    }
    case Actions.FACEBOOK_API_UNSET: {
      return applyUnsetFacebookAPI(state);
    }
    case Actions.FACEBOOK_API_SELECTED_AD_ACCOUNT_ID: {
      return applyUnsetFacebookAPISelectedAccountID(state, action);
    }
    default: 
      return state;
  }
}

export default facebookAPIReducer;
