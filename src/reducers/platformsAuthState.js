// ------------ LOCAL DEPENDENCIES ------------ //

import * as Actions from '../constants/ActionTypes';
import * as PlatformIDs from '../constants/PlatformIDs';

// ------------ Constants & Global Variables ------------ //

const INIT_STATE = {
  [PlatformIDs.FACEBOOK] : false,
  [PlatformIDs.INSTAGRAM]: false,
  [PlatformIDs.LINKED_IN] : false,
  [PlatformIDs.GOOGLE]   : false,
}

// ------------- MAIN ------------- //

const applySetPlatformsState = (state, action) => ({
  ...state,
  [action.platformID]: action.platformAuthState,
});

function platformsStateReducer(state = INIT_STATE, action) {
  switch (action.type) {
    case Actions.PLATFORMS_AUTH_STATE_SET: return applySetPlatformsState(state, action);
    default: return state;
  }
}

export default platformsStateReducer;
