// ------------ LOCAL DEPENDENCIES ------------ //

import * as Actions from '../constants/ActionTypes';
import * as PlatformIDs from '../constants/PlatformIDs';
import * as ErrorCodesEnum from '../constants/ErrorCodesEnum';

// ------------ Constants & Global Variables ------------ //

const INIT_STATE = {
  authErrorID     : PlatformIDs.NONE,
  authErrorMessage: '',
  authErrorCode   : null,
}

// ------------- MAIN ------------- //

const getErrorMessage = (platformID, errorCode) => {
  switch(platformID) {
    case PlatformIDs.FACEBOOK: {
      switch(errorCode) {
        case ErrorCodesEnum.FACEBOOK_O_AUTH_ERROR  : return 'Please refresh the session by re-logging into Facebook.';
        case ErrorCodesEnum.FACEBOOK_NO_ACCESS_CODE: return 'Please refresh the session by re-logging into Facebook.';
        default: return '';
      }
    }
    default: return '';
  }
}

const applySetPlatformAuthError = (state, action) => ({
  ...state,
  authErrorID     : action.authErrorID,
  authErrorCode   : action.authErrorCode,
  authErrorMessage: getErrorMessage(action.authErrorID, action.authErrorCode),
});

const applyClearPlatformAuthError = () => (INIT_STATE);

function platformAuthErrorReducer(state = INIT_STATE, action) {
  switch (action.type) {
    case Actions.PLATFORM_AUTH_ERROR_SET  : return applySetPlatformAuthError(state, action);
    case Actions.PLATFORM_AUTH_ERROR_CLEAR: return applyClearPlatformAuthError();
    default: return state;
  }
}

export default platformAuthErrorReducer;
