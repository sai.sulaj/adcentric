// ------------- LOCAL DEPENDENCIES -------------- //

import * as Actions from '../constants/ActionTypes';

// ------------- CONSTANTS & GLOBAL VARS -------------- //

const INITIAL_STATE = {
  facebookUser: null,
};

// ------------- MAIN -------------- //

const applySetFacebookUser = (_State, _Action) => ({
  ..._State,
  facebookUser: _Action.facebookUser,
});

function facebookUserReducer(_State = INITIAL_STATE, _Action) {
  switch (_Action.type) {
    case Actions.FACEBOOK_USER_SET: {
      return applySetFacebookUser(_State, _Action);
    }
    default:
      return _State;
  }
}

export default facebookUserReducer;
