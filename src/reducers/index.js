// ------------- DEPENDENCIES -------------- //

import { combineReducers } from 'redux';

// ------------- LOCAL DEPENDENCIES -------------- //

import sessionReducer from './session';
import facebookUserReducer from './facebookUser';
import facebookAPIReducer from './FacebookAPI';
import settingsReducer from './settings';
import platformAuthErrorReducer from './platformAuthError';
import platformsAuthStateReducer from './platformsAuthState';

// ------------- MAIN -------------- //

const rootReducer = combineReducers({
  sessionState          : sessionReducer,
  facebookUserState     : facebookUserReducer,
  facebookAPIState      : facebookAPIReducer,
  settingsState         : settingsReducer,
  platformAuthErrorState: platformAuthErrorReducer,
  platformsAuthState    : platformsAuthStateReducer,
});

export default rootReducer;
