// ---------- DEPENDENCIES ---------- //

import joi from 'joi';

// ---------- SCHEMAS ---------- //

const facebookDataObjectSchema = joi.object().keys({
  name       : joi.string().required(),
  userID     : joi.string().required(),
  accessToken: joi.string().required(),
});

// ---------- MAIN ---------- //

/**
 * Validates facebook user data object. Returns an array of 2 values: error which is
 * null if the object is valid, and value which is the original object.
 *
 * @param {JSON} object -> facebook user object to validate.
 *
 * @return {JSON} -> A JSON object containing an error field, and a value field.
 */
export const facebookDataObject = (object) =>
  joi.validate(object, facebookDataObjectSchema);