// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';
import {
  Dialog,
  DialogTitle,
  withStyles,
} from '@material-ui/core';

// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './ConnectAdAccountDialog.css';

// Components.

import AdAccountsList from '../AdAccountsList';

// --------------- CONSTANTS --------------- //

const materialStyles = {
  root: {},
  title: {
    '& > h2': {
      fontSize: 20,
    },
  },
};

// --------------- MAIN --------------- //

class ConnectAdAccountDialog extends Component {
  render() {
    const {
      classes,
      open,       // {Boolean} -> Component is rendered when true.
      accounts,   // {Array<String>} -> List of ad account IDs.
      onClose,    // {function} -> Called on dialog close.
      onSelect,   // {function} -> Called on ad account select.
    } = this.props

    return (
      <Dialog fullWidth
        className={classes.root} open={open} onClose={onClose}>
        <DialogTitle className={classes.title}>Connect a Facebook Ad Account</DialogTitle>
        <AdAccountsList onSelect={onSelect} adAccounts={accounts} />
      </Dialog>
    );
  }
}

export default withStyles(materialStyles)(ConnectAdAccountDialog);
