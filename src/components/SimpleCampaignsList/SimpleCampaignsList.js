// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';
import { compose } from 'recompose';
import {
  Paper,
  withStyles,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  Button,
  Checkbox,
} from '@material-ui/core';

// --------------- LOCAL DEPENDENCIES --------------- //

import './SimpleCampaignsList.css';

// --------------- CONSTANTS --------------- //

const materialStyles = {
  paper: {
    height   : '100%',
    width    : '100%',
    textAlign: 'center',
  },
  header: {
    padding     : '10px 0 10px 10px',
    margin      : '0 !important',
    background  : '#373a47',
    color       : '#FFFFFF',
    borderRadius: '4px 4px 0 0',
  },
  listRoot: {
    overflow: 'auto',
    height  : '35vh',
  },
  selectAllButton: {
    marginTop: 5,
    width    : '90%',
    '&:hover': {
      textDecoration        : 'none',
      backgroundColor       : '#F45B691F',
      '@media (hover: none)': {
        backgroundColor: 'transparent',
      },
      '&$disabled': {
        backgroundColor: 'transparent',
      },
    }
  }
};

const INIT_STATE = {
  checked: [],
};

// --------------- MAIN --------------- //

class SimpleCampaignsList extends Component {
  constructor(props) {
    super(props);

    this.state = INIT_STATE;
  }

  checkAll = () => {
    const { campaigns } = this.props;
    const { checked }   = this.state;

    if (checked.length > 0) {
      this.setState({
        checked: [],
      });
    } else {
      this.setState({
        checked: campaigns.map((campaign) => {
          return campaign.number;
        })
      });
    }
  }

  handleToggle = value => () => {
    const { checked } = this.state;
    const currIndex   = checked.indexOf(value);
    const newChecked  = [...checked];

    if (currIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currIndex, 1);
    }

    this.setState({
      checked: newChecked,
    });
  }

  render() {
    const { checked }            = this.state;
    const { classes, campaigns } = this.props;

    return (
      <Paper className={classes.paper} elevation={2}>
        <h4 className={classes.header}>Campaigns</h4>
        <List className={classes.listRoot}>
          {(campaigns !== undefined && campaigns !== null ? campaigns : []).map((campaign, i) => {
            return  (
              <ListItem key={i}>
                <ListItemText primary={`Campaign ${campaign.number}`} />
                <ListItemSecondaryAction>
                  <Checkbox
                    onChange = {this.handleToggle(campaign.number)}
                    checked  = {checked.indexOf(campaign.number) !== -1} />
                </ListItemSecondaryAction>
              </ListItem>
            );
          })}
        </List>
        <Button onClick={this.checkAll} className={classes.selectAllButton}>
          {checked.length > 0 ? 'Clear' : 'Select all'}
        </Button>
      </Paper>
    );
  }
}

export default compose(
  withStyles(materialStyles),
)(SimpleCampaignsList);
