// --------------- DEPENDENCIES --------------- //

import React, { PureComponent } from 'react';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import {
  Button,
  withStyles,
  Paper,
} from '@material-ui/core';
import {
  CheckCircleRounded,
  HighlightOffRounded,
} from '@material-ui/icons';

// --------------- LOCAL DEPENDENCIES --------------- //

// Icons.

import facebookIcon from '../../assets/platformIcons/facebook.svg';
import instagramIcon from '../../assets/platformIcons/instagram.svg';
import linkedInIcon from '../../assets/platformIcons/linkedin.svg';
import googleIcon from '../../assets/platformIcons/google.svg';

// CSS.

import './PlatformsStatusTabs.css';

// CONSTANTS.

import * as SettingsTabs from '../../constants/SettingsTabs';

// --------------- CONSTANTS --------------- //

const materialStyles = {
  paper: {
    width          : '100%',
    height         : '100%',
    padding        : 4,
    backgroundColor: '#373a47',
  },
  buttonBase: {
    marginRight: 5,
    fontSize   : 12,
  },
  icon: {
    marginLeft: 8,
  },
  facebookButton: {
    background: '#3B5998',
    color     : '#FFFFFF',
  },
  instagramButton: {
    background  : 'linear-gradient(90deg, rgba(251,57,88,1) 50%, rgba(255,200,56,1) 100%)',
    color       : '#FFFFFF',
    '&:disabled': {
      color     : '#828282',
      background: '#4C4C4C',
    },
    '&:disabled img': {
      opacity: 0.3,
    },
  },
  linkedInButton: {
    backgroundColor: '#0077b5',
    color          : '#FFFFFF',
    '&:disabled'   : {
      color     : '#828282',
      background: '#4C4C4C',
    },
    '&:disabled img': {
      opacity: 0.3,
    },
  },
  googleButton: {
    background  : '#F3F3F3',
    color       : '#373a47',
    '&:disabled': {
      color     : '#828282',
      background: '#4C4C4C',
    },
    '&:disabled img': {
      opacity: 0.3,
    },
  },
  buttonWrapper: {
    display       : 'flex',
    flexFlow      : 'row nowrap',
    justifyContent: 'center',
    alignItems    : 'center',
  },
  green: {
    color: '#218838',
  },
  red: {
    color: '#dc3545',
  },
};

const ICON_WIDTH = 16;      // Platform icon width in pixels.

// --------------- MAIN --------------- //

class PlatformsStatusTabs extends PureComponent {
  /**
   * Navigated to platforms settings page on button click.
   *
   * @param {String} platformID -> Clicked platform ID.
   */
  handleNav = (platformID) => {
    const { onNav } = this.props;

    if (onNav !== null && onNav !== undefined) {
      onNav(SettingsTabs.PLATFORMS_TAB);
    }
  }

  render() {
    const {
      classes,
      facebookAvailable,
      instagramAvailable,
      linkedInAvailable,
      googleAvailable,
    } = this.props;

    return (
      <Paper className={ [classes.paper, 'buttons-wrapper'].join(' ') } elevation={2}>
        <div className={classes.buttonWrapper}>
          <Button size='small' variant="contained"
            className = {[classes.buttonBase, classes.facebookButton].join(' ')}
            onClick   = {() => this.handleNav('facebook')}>
            Facebook
            <img alt='' className={classes.icon} src={facebookIcon} width={ICON_WIDTH}/>
          </Button>
          { facebookAvailable
            ? <CheckCircleRounded className={classes.green} />
            :  <HighlightOffRounded className={classes.red} />
          }
        </div>
        <div className={classes.buttonWrapper}>
          <Button size='small' variant="contained"
            className = {[classes.buttonBase, classes.instagramButton].join(' ')}
            onClick   = {() => this.handleNav('instagram')}>
            Instagram
            <img alt='' className={classes.icon} src={instagramIcon} width={ICON_WIDTH}/>
          </Button>
          { instagramAvailable
            ? <CheckCircleRounded className={classes.green} />
            :      <HighlightOffRounded className={classes.red} />
          }
        </div>
        <div className={classes.buttonWrapper}>
          <Button size='small' variant="contained"
            className = {[classes.buttonBase, classes.linkedInButton].join(' ')}
            onClick   = {() => this.handleNav('linkedin')}>
            LinkedIn
            <img alt='' className={classes.icon} src={linkedInIcon} width={ICON_WIDTH}/>
          </Button>
          { linkedInAvailable
            ? <CheckCircleRounded className={classes.green} />
            :      <HighlightOffRounded className={classes.red} />
          }
        </div>
        <div className={classes.buttonWrapper}>
          <Button size='small' variant="contained"
            className = {[classes.buttonBase, classes.googleButton].join(' ')}
            onClick   = {() => this.handleNav('google')}>
            Google
            <img alt='' className={classes.icon} src={googleIcon} width={ICON_WIDTH}/>
          </Button>
          { googleAvailable
            ? <CheckCircleRounded className={classes.green} />
            : <HighlightOffRounded className={classes.red} />
          }
        </div>
      </Paper>
    );
  }
}

const mapStateToProps = state => ({
  facebookAvailable: state.facebookAPIState.available,
});

export default compose(
  withStyles(materialStyles),
  connect(mapStateToProps, null),
)(PlatformsStatusTabs);
