// --------------- DEPENDENCIES --------------- //

import React, {
  PureComponent,
} from 'react';
import {
  Paper,
  withStyles,
} from '@material-ui/core';
import {
  LocationOnRounded,
} from '@material-ui/icons';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import Avatar from 'react-avatar';

// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './ProfileSettingsComponents.css';

// --------------- CONSTANTS --------------- //

const profileMainComponentStyles = theme => ({
  profileMainComponentRoot: {
    width              : '100%',
    height             : '100%',
    padding            : 10,
    display            : 'grid',
    gridTemplateColumns: '1fr 3fr',
    gridTemplateAreas  : '"profile-main-component-col-1 profile-main-component-col-2"',
  },
  profileMainComponentAvatar: {
  },
  profileMainComponentDisplayName: {
    color     : '#686868',
    fontWeight: '600 !important',
    fontSize  : 30,
    margin    : 0,
  },
  profileMainComponentLocationWrapper: {
    display       : 'flex',
    flexFlow      : 'row nowrap',
    justifyContent: 'center',
    alignItems    : 'center',
    '& > *'       : {
      color: '#686868',
    },
    '& > svg': {
      fontSize: 20,
    }
  },
  profileMainComponentLocationText: {
    margin: '0 0 0 5px',
  },
  profileMainComponentEmail: {
    color     : '#686868',
    paddingTop: 20,
  },
});

const simpleProfileDataComponentStyles = theme => ({
  simpleProfileDataComponentRoot: {
    width   : '100%',
    height  : '100%',
    padding : 10,
    position: 'relative',
  },
  simpleProfileDataComponentWrapper: {},
  simpleProfileDataComponentData   : {
    display       : 'flex',
    flexFlow      : 'column nowrap',
    justifyContent: 'center',
    alignItems    : 'center',
  },
  simpleProfileDataComponentValue: {
    fontSize  : 60,
    fontWeight: '300 !important',
  },
  simpleProfileDataComponentLabel: {
    fontWeight: '300 !important',
    color     : '#606060',
  },
  simpleProfileDataComponentPrefix: {
    fontSize  : 50,
    fontWeight: '300 !important',
  },
  simpleProfileDataComponentIconWrapper: {
    [theme.breakpoints.down('md')]: {
      display: 'none',
    },
    marginLeft: '20px',
    position  : 'relative',
    top       : '50%;',
    transform : 'translateY(-50%)',
    '& > svg' : {
      fontSize: 30,
    }
  }
});

const METRIC_PREFIX_ENUM = {
  0: '',
  1: 'K',   // Kilo.
  2: 'M',   // Mega.
  3: 'G',   // Giga.
};

// --------------- ProfileMainComponentBase --------------- //

class ProfileMainComponentBase extends PureComponent {
  render() {
    const {
      classes,
      facebookUser,
      authUser,
      location,
    } = this.props;

    return (
      <Paper className={classes.profileMainComponentRoot} elevation={2}>
        <div className='profile-main-component-col-1'>
          <Avatar className={classes.profileMainComponentAvatar}
            facebookId={facebookUser !== null ? facebookUser.id : null} size={120} round />
        </div>
        <div className='profile-main-component-col-2'>
          <p className={classes.profileMainComponentDisplayName}>{facebookUser !== null ? facebookUser.userName : null}</p>
          <div className={classes.profileMainComponentLocationWrapper}>
            <LocationOnRounded className={classes.profileMainComponentLocationIcon} />
            <p className={classes.profileMainComponentLocationText}>{location}</p>
          </div>
          <p className={classes.profileMainComponentEmail}>{authUser.email}</p>
        </div>
      </Paper>
    );
  }
}

const mapProfileMainComponentStateToProps = state => ({
  facebookUser: state.facebookUserState.facebookUser,
  authUser    : state.sessionState.authUser,
});

const ProfileMainComponent = compose(
  withStyles(profileMainComponentStyles),
  connect(mapProfileMainComponentStateToProps, null),
)(ProfileMainComponentBase);

// --------------- SimpleProfileDataComponentBase --------------- //

class SimpleProfileDataComponentBase extends PureComponent {
  /**
   * Formats metric value. Appends '%' if 'percentage' prop is true.
   * Otherwise returns string of number with a max of 3 significant
   * digits, and appends 'K', 'M', 'G'.
   *
   * @param {number} value -> Number to format.
   *
   * @returns {String} -> Formatted value.
   */
  getFormattedValue = (value) => {
    const { percentage } = this.props;
    if (value === 0) {
      return '0';
    } else if (percentage) {
      return `${Math.round(value * 100)}`;
    } else if (typeof value === 'number') {
      const pow       = Math.log10(value);
      const postPendI = Math.floor(pow / 3);
      const fVal      = Math.round(value / Math.pow(10, postPendI * 3));
      
      return `${fVal}`;
    } else {
      return value;
    }
  }

  /**
   * Calculates whether 'K', 'M', 'G', '%', or no suffix is to be appended to value
   * and returns correct suffix.
   *
   * @param {number} value -> Value to reference for calculation.
   *
   * @returns {String} -> Correct suffix.
   */
  getMetricPrefix = (value) => {
    const { percentage } = this.props;
    if (value === 0) {
      return '';
    } else if (percentage) {
      return `%`;
    } else if (typeof value === 'number') {
      const pow       = Math.log10(value);
      const postPendI = Math.floor(pow / 3);
      
      return `${METRIC_PREFIX_ENUM[postPendI]}`;
    } else {
      return '';
    }
  }

  render() {
    const {
      classes,
      label,
      value,
      icon,
    } = this.props;

    return (
      <Paper className={classes.simpleProfileDataComponentRoot} elevation={2}>
        <div className={classes.simpleProfileDataComponentIconWrapper}>{icon}</div>
        <div className={classes.simpleProfileDataComponentData}>
          <div className={classes.simpleProfileDataComponentWrapper}>
            <span className={classes.simpleProfileDataComponentValue}>{this.getFormattedValue(value)}</span>
            <span className={classes.simpleProfileDataComponentPrefix}>{this.getMetricPrefix(value)}</span>
          </div>
          <span className={classes.simpleProfileDataComponentLabel}>{label}</span>
        </div>
      </Paper>
    );
  }
}

const SimpleProfileDataComponent = compose(
  withStyles(simpleProfileDataComponentStyles),
)(SimpleProfileDataComponentBase);

export {
  ProfileMainComponent,
  SimpleProfileDataComponent,
};

export default {};
