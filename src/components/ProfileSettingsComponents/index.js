import {
  ProfileMainComponent,
  SimpleProfileDataComponent,
} from './ProfileSettingsComponents';

export {
  ProfileMainComponent,
  SimpleProfileDataComponent,
};

export default {};