// ------------- DEPENDENCIES ------------- //

import React from 'react';
import Auth from 'react-facebook-auth';

// ------------- MAIN ------------- //

const FacebookAuth = ({ trigger, callback, onFailure }) => (
  <Auth
    appId       = {process.env.REACT_APP_FACEBOOK_APP_ID}
    callback    = {callback}
    component   = {trigger}
    onFailure   = {onFailure}
    scope       = {['ads_read', 'ads_management']}
    redirectURI = {process.env.REACT_APP_FACEBOOK_AUTH_CALLBACK_URL}
  />
);

export default FacebookAuth;
