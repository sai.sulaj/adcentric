// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';
import {
  Stepper,
  Step,
  StepLabel,
  StepContent,
  Typography,
  withStyles,
} from '@material-ui/core';

// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './LaunchStepper.css';

// --------------- CONSTANTS --------------- //

const materialStyles = {
  root: {
    width: '100%',
  },
};

const INIT_STATE = {
  steps: [      // {Array<Object>} -> Array of step title and description.
    {
      title: 'Create a Campaign',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nulla facilisi cras fermentum odio eu feugiat pretium nibh. Sed augue lacus viverra vitae congue eu consequat.',
    },
    {
      title: 'Set a Budget',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nulla facilisi cras fermentum odio eu feugiat pretium nibh. Sed augue lacus viverra vitae congue eu consequat.',
    },
    {
      title: 'Customize your Ad',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nulla facilisi cras fermentum odio eu feugiat pretium nibh. Sed augue lacus viverra vitae congue eu consequat.',
    },
  ],
};

// --------------- MAIN --------------- //

class LaunchStepper extends Component {
  constructor(props) {
    super(props);

    this.state = INIT_STATE;
  }

  render() {
    const {
      classes,
      activeStep,
    } = this.props;
    const {
      steps,
    } = this.state;

    return (
      <div className={classes.root}>
        <Stepper activeStep={activeStep} orientation='vertical'>
          {steps.map((step, index) => (
            <Step key={index}>
              <StepLabel>{step.title}</StepLabel>
              <StepContent>
                <Typography>{step.content}</Typography>
              </StepContent>
            </Step>
          ))}
        </Stepper>
      </div>
    );
  }
}

export default withStyles(materialStyles)(LaunchStepper);
