// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';

// --------------- LOCAL DEPENDENCIES --------------- //

import './AudienceFeatureLabelValue.css';

// --------------- CONSTANTS --------------- //


// --------------- MAIN --------------- //

class AudienceFeatureLabelValue extends Component {
  render() {
    const {
      label,
      value,
      color,
    } = this.props;

    return (
      <div className='audience-feature-lv-root'>
        <span className='audience-feature-lv-value' style={{ color: color }}>{value}</span>
        <span className='audience-feature-lv-label'>{label.toUpperCase()}</span>
      </div>
    );
  }
}

export default AudienceFeatureLabelValue;
