// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';
import { compose } from 'recompose';
import {
  withStyles,
  Paper,
} from '@material-ui/core';

// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './SimpleDemographicsOverview.css';

// Components.

import AudienceFeatureLabelValue from '../AudienceFeatureLabelValue';

// --------------- CONSTANTS --------------- //

const materialStyles = {
  paper: {
    width  : '100%',
    height : '100%',
    padding: 10,
  }
}

// --------------- MAIN --------------- //

class SimpleDemographicsOverview extends Component {
  render() {
    const { classes } = this.props;

    return (
      <Paper className={ classes.paper } elevation={2}>
        <h4>Demographics</h4>
        <div className='audience-features-wrapper'>
          <AudienceFeatureLabelValue label={'age'} value={'20 - 30'} />
          <AudienceFeatureLabelValue label={'age'} value={'60+'} />
          <AudienceFeatureLabelValue valueColor={'#0000FF'} label={'sex'} value={'MALE'} />
          <AudienceFeatureLabelValue label={'income'} value={'60k - 80k'} />
        </div>
      </Paper>
    );
  }
}

export default compose(
  withStyles(materialStyles),
)(SimpleDemographicsOverview);
