// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';
import {
  Paper,
  withStyles,
  List,
  ListItem,
  ListItemText,
  Checkbox,
} from '@material-ui/core';
import { compose } from 'recompose';

// --------------- LOCAL DEPENDENCIES --------------- //

import './MetricsSelector.css';

// --------------- CONSTANTS --------------- //

const metricsSelectorStyles = {
  metricsSelectorRoot: {
    height: '100%',
    width : '100%',
  },
  metricsSelectorList: {
    height  : '100%',
    overflow: 'auto',
  },
  metricsSelectorListItemText: {
    fontSize: 12,
  },
  metricsSelectorHeading: {
    padding     : '15px 0 15px 10px',
    margin      : '0 !important',
    background  : '#373a47',
    color       : '#FFFFFF',
    borderRadius: '4px 4px 0 0',
  },
  metricsSelectorListItem: {
    padding: '0 5px',
  },
  metricsSelectorListItemCheckbox: {
    padding: 12,
  },
};

const INIT_STATE = {
  checked: [],      // Array of indices of checked metrics.
};

// --------------- MAIN --------------- //

class MetricsSelector extends Component {
  constructor(props) {
    super(props);

    this.state = INIT_STATE;
  }

  /**
   * Handles metric select.
   */
  handleClick = (metric) => {
    this.setState(prevState => {
      if (prevState.checked.includes(metric)) {
        return {
          checked: prevState.checked.filter(m => m !== metric)
        };
      } else {
        return {
          checked: [...prevState.checked, metric]
        };
      }
    });
  }

  render() {
    const {
      classes,
      metrics,
    } = this.props;
    const { checked } = this.state;
    const vMetrics    = metrics !== null && metrics !== undefined ? metrics : [];

    return (
      <Paper className={classes.metricsSelectorRoot} elevation={2}>
        <h4 className={classes.metricsSelectorHeading}>Metrics</h4>
        <List className={classes.metricsSelectorList}>
          {vMetrics.map((metric, i) => (
            <ListItem className={classes.metricsSelectorListItem} key={i} onClick={() => this.handleClick(metric)} dense button>
              <ListItemText className={classes.metricsSelectorListItemText} primary={metric} />
              <Checkbox
                className = {classes.metricsSelectorListItemCheckbox}
                checked   = {checked.includes(metric)}
                tabIndex  = {-1}
                disableRipple />
            </ListItem>
          ))}
        </List>
      </Paper>
    );
  }
}

export default compose(
  withStyles(metricsSelectorStyles),
)(MetricsSelector);
