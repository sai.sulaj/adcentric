// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import {
  Button,
  withStyles,
  Paper,
} from '@material-ui/core';

// --------------- LOCAL DEPENDENCIES --------------- //

// Icons.

import facebookIcon from '../../assets/platformIcons/facebook.svg';
import instagramIcon from '../../assets/platformIcons/instagram.svg';
import linkedInIcon from '../../assets/platformIcons/linkedin.svg';
import googleIcon from '../../assets/platformIcons/google.svg';

// CSS.

import './PlatformsTabs.css';

// Constants.

import * as PlatformIDs from '../../constants/PlatformIDs';

// --------------- CONSTANTS --------------- //

const materialStyles = {
  paper: {
    width          : '100%',
    height         : '100%',
    padding        : 4,
    backgroundColor: '#373a47',
  },
  buttonBase: {
    marginRight: 5,
    fontSize   : 12,
  },
  icon: {
    marginLeft: 8,
  },
  facebookButton: {
    background: '#3B5998',
    color     : '#FFFFFF',
  },
  instagramButton: {
    background  : 'linear-gradient(90deg, rgba(251,57,88,1) 50%, rgba(255,200,56,1) 100%)',
    color       : '#FFFFFF',
    '&:disabled': {
      color     : '#828282',
      background: '#4C4C4C',
    },
    '&:disabled img': {
      opacity: 0.3,
    },
  },
  linkedInButton: {
    backgroundColor: '#0077b5',
    color          : '#FFFFFF',
    '&:disabled'   : {
      color     : '#828282',
      background: '#4C4C4C',
    },
    '&:disabled img': {
      opacity: 0.3,
    },
  },
  googleButton: {
    background  : '#F3F3F3',
    color       : '#373a47',
    '&:disabled': {
      color     : '#828282',
      background: '#4C4C4C',
    },
    '&:disabled img': {
      opacity: 0.3,
    },
  },
};

const ICON_WIDTH = 16;      // Platform icon width in pixels.

const INIT_STATE = {
  selectedPlatform: null,   // Selected platform name.
};

// --------------- MAIN --------------- //

class PlatformsTabs extends Component {
  constructor(props) {
    super(props);

    this.state = INIT_STATE;
  }

  /**
   * Handles platform select.
   *
   * @param {String} platformName -> Clicked platform name.
   */
  doPlatformSelect = (platformName) => {
    const { onPlatformSelect } = this.props;
    const nameLower            = platformName.toLowerCase();

    this.setState({
      selectedPlatform: nameLower,
    });
    if (onPlatformSelect !== null && onPlatformSelect !== undefined) {
      onPlatformSelect(nameLower);
    }
  }

  render() {
    const {
      classes,
      platformsAuthState,
    } = this.props;

    return (
      <Paper className={ [classes.paper, 'buttons-wrapper'].join(' ') } elevation={2}>
        <Button size='small' variant="contained"
          className = {[classes.buttonBase, classes.facebookButton].join(' ')}
          onClick   = {() => this.doPlatformSelect(PlatformIDs.FACEBOOK)}
          disabled  = {!platformsAuthState[PlatformIDs.FACEBOOK]}>
          Facebook
          <img alt='' className={classes.icon} src={facebookIcon} width={ICON_WIDTH}/>
        </Button>
        <Button size='small' variant="contained"
          className = {[classes.buttonBase, classes.instagramButton].join(' ')}
          onClick   = {() => this.doPlatformSelect(PlatformIDs.INSTAGRAM)}
          disabled  = {!platformsAuthState[PlatformIDs.INSTAGRAM]}>
          Instagram
          <img alt='' className={classes.icon} src={instagramIcon} width={ICON_WIDTH}/>
        </Button>
        <Button size='small' variant="contained"
          className = {[classes.buttonBase, classes.linkedInButton].join(' ')}
          onClick   = {() => this.doPlatformSelect(PlatformIDs.LINKED_IN)}
          disabled  = {!platformsAuthState[PlatformIDs.LINKED_IN]}>
          LinkedIn
          <img alt='' className={classes.icon} src={linkedInIcon} width={ICON_WIDTH}/>
        </Button>
        <Button size='small' variant="contained"
          className = {[classes.buttonBase, classes.googleButton].join(' ')}
          onClick   = {() => this.doPlatformSelect(PlatformIDs.GOOGLE)}
          disabled  = {!platformsAuthState[PlatformIDs.GOOGLE]}>
          Google
          <img alt='' className={classes.icon} src={googleIcon} width={ICON_WIDTH}/>
        </Button>
      </Paper>
    );
  }
}

const mapStateToProps = state => ({
  facebookAvailable : state.facebookAPIState.available,
  platformsAuthState: state.platformsAuthState,
});

export default compose(
  withStyles(materialStyles),
  connect(mapStateToProps, null),
)(PlatformsTabs);
