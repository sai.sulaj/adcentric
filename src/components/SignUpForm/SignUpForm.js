// ------------- DEPENDENCIES ------------- //

import React, { Component } from 'react';
import {
  FormGroup,
  ControlLabel,
  FormControl,
  InputGroup,
  Button,
  Glyphicon,
} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';

// ------------- LOCAL DEPENDENCIES ------------- //

// CSS.
import './SignUpForm.css';

// Constants.

import * as Routes from '../../constants/Routes';

// Components.

import { Auth, Db } from '../../firebase';

// ------------- MAIN ------------- //

// Constants.

const INIT_STATE = {
  // Initial state of component.
  email                     : '',     // User's email.
  passwordOne               : '',     // User's new password.
  passwordTwo               : '',     // User's new password confirmation.
  error                     : null,   // Sign up error.
  emailValidationState      : null,
  passwordOneValidationState: null,
  passwordTwoValidationState: null,
};

class SignUpForm extends Component {
  constructor(props) {
    super(props);

    this.state = INIT_STATE;
  }

  /**
   * Accesses email value in state, and computes validation
   * state for bootstrap FormGroup field. TODO: Figure out wtf
   * the regex expression means.
   *
   * @param {String} email -> Email value.
   *
   * @return -> 'success' if valid, 'error' otherwise.
   */
  emailValidationState = email => {
    // eslint-disable-next-line
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    return re.test(`${email}`.toLowerCase()) ? 'success': 'error';
  };

  /**
   * Accesses passwordOne value in state, and computes validation
   * state for bootstrap FormGroup field. Valid if field is
   * not an empty string.
   *
   * @param {String} passwordOne -> Password value.
   *
   * @return -> 'success' if valid, 'error' otherwise.
   */
  passwordOneValidationState = passwordOne => {
    return passwordOne !== '' ? 'success': 'error';
  };

  /**
   * Accesses passwordTwo value in state, and computes validation
   * state for bootstrap FormGroup field. Valid if field is
   * not an empty string.
   *
   * @param {String} passwordTwo -> Password value.
   *
   * @return -> 'success' if valid, 'error' otherwise.
   */
  passwordTwoValidationState = passwordTwo => {
    return passwordTwo !== '' ? 'success': 'error';
  };

  /**
   * Set's email value in component state,
   * and updates emailValidationState in component state.
   *
   * @param {String} value -> Value to set in component state.
   */
  handleEmailFieldChange = value => {
    this.setState({
      email               : value,
      emailValidationState: this.emailValidationState(value),
    });
  };

  /**
   * Set's passwordOne value in component state,
   * and updates passwordOneValidationState in component state.
   *
   * @param {String} value -> Value to set in component state.
   */
  handlePasswordOneFieldChange = value => {
    this.setState({
      passwordOne               : value,
      passwordOneValidationState: this.passwordOneValidationState(value),
    });
  };

  /**
   * Set's passwordTwo value in component state,
   * and updates passwordTwoValidationState in component state.
   *
   * @param {String} value -> Value to set in component state.
   */
  handlePasswordTwoFieldChange = value => {
    this.setState({
      passwordTwo               : value,
      passwordTwoValidationState: this.passwordTwoValidationState(value),
    });
  };

  /**
   * Fetches email and passwordOne values im component state, signs user
   * up to Firebase Auth, and creates user in Firebase Firestore. Does
   * no input validation. On success, navigates to OVERVIEW page. On
   * error, sets 'error' value in component state with returned error.
   * Meant for 'onClick' button event callback.
   *
   * @param {JSON} event -> 'onClick' button event data object.
   */
  onSubmit = event => {
    const { email, passwordOne } = this.state;
    const { history }            = this.props;

    Auth.doCreateUserWithEmailAndPassword(email, passwordOne)
      .then(authUser => {
        // Create user in Firebase Database.
        Db.doCreateUser(authUser.user.uid, email)
          .then(() => {
            this.setState({ ...INIT_STATE });
            history.push(Routes.OVERVIEW);
          })
          .catch(error => {
            this.setState({
              error: error,
            });
          });
      })
      .catch(error => {
        this.setState({
          error: error,
        });
      });

    event.preventDefault();
  };

  render() {
    const {
      email,
      passwordOne,
      passwordTwo,
      error,
      emailValidationState,
      passwordOneValidationState,
      passwordTwoValidationState,
    } = this.state;

    // For submit button 'disabled' prop.
    const isValid = 
      emailValidationState === 'success' &&
      passwordOneValidationState === 'success' &&
      passwordTwoValidationState === 'success';

    return (
      <form className="form-horizontal">
        <FormGroup controlId="signup-email-field" validationState={emailValidationState}>
          <ControlLabel className="signup-form-label">Your Email</ControlLabel>
          <InputGroup>
            <InputGroup.Addon>
              <Glyphicon glyph="envelope" />
            </InputGroup.Addon>
            <FormControl
              autoComplete = "email"
              type         = "email"
              value        = {email}
              placeholder  = "Email"
              onChange     = {evt => this.handleEmailFieldChange(evt.target.value)}
            />
            <FormControl.Feedback />
          </InputGroup>
        </FormGroup>

        <FormGroup
          controlId       = "signup-password-one-field"
          validationState = {passwordOneValidationState}
        >
          <ControlLabel className="signup-form-label">Create Password</ControlLabel>
          <InputGroup>
            <InputGroup.Addon>
              <Glyphicon glyph="lock" />
            </InputGroup.Addon>
            <FormControl
              type        = "password"
              value       = {passwordOne}
              placeholder = "Create password"
              onChange    = {evt => this.handlePasswordOneFieldChange(evt.target.value)}
            />
            <FormControl.Feedback />
          </InputGroup>
        </FormGroup>

        <FormGroup
          controlId       = "signup-password-two-field"
          validationState = {passwordTwoValidationState}>
          <ControlLabel className="signup-form-label">Confirm Password</ControlLabel>
          <InputGroup>
            <InputGroup.Addon>
              <Glyphicon glyph="lock" />
            </InputGroup.Addon>
            <FormControl
              type        = "password"
              value       = {passwordTwo}
              placeholder = "Confirm password"
              onChange    = {evt => this.handlePasswordTwoFieldChange(evt.target.value)}
            />
            <FormControl.Feedback />
          </InputGroup>
        </FormGroup>

        <Button
          type      = "submit"
          bsStyle   = "primary"
          className = "signup-submit-button"
          disabled  = {!isValid}
          onClick   = {this.onSubmit}>
          Sign Up
        </Button>

        <div className="signup-signin-link-wrapper">
          <Link className="signup-signin-link" to={Routes.SIGN_IN}>
            Sign In
          </Link>
        </div>

        {error && <p>{error.message}</p>}
      </form>
    );
  }
}

const Standalone = SignUpForm;
export { Standalone };

export default compose(withRouter)(SignUpForm);
