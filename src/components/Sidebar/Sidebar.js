// ---------- DEPENDENCIES ---------- //

import React, { Component } from 'react';
import { NavLink, withRouter, } from 'react-router-dom';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import Avatar from 'react-avatar';
import {
  Tooltip,
} from '@material-ui/core';

// ---------- LOCAL DEPENDENCIES ---------- //

// Constants.

import * as Routes from '../../constants/Routes';
import * as Actions from '../../constants/ActionTypes';
import * as SettingsTabs from '../../constants/SettingsTabs';

// CSS.

import './Sidebar.css';

// SVG.
import homeIcon from '../../assets/sidebar-home-icon.svg';
import launchIcon from '../../assets/sidebar-launch-icon.svg';
import campaignsIcon from '../../assets/sidebar-campaigns-icon.svg';
import demographicsIcon from '../../assets/sidebar-demographics-icon.svg';
import settingsIcon from '../../assets/sidebar-settings-icon.svg';
import helpIcon from '../../assets/sidebar-help-icon.svg';

// ---------- MAIN ---------- //

const SIDEBAR_ICON_WIDTH = 22;

class Sidebar extends Component {
  render() {
    const {
      facebookUser,
      onSetDefaultSettingsPage,
    } = this.props;

    return (
      <div className='sidebar-content-wrapper'>
        <div className='sidebar-content sidebar-content-top'>
          <NavLink className='sidebar-link logo-link' to={Routes.OVERVIEW} exact>
            <div className='sidebar-link-content-wrapper'>
              <img alt='' className='sidebar-link-icon' src='/logo-main-compressed.png' width={SIDEBAR_ICON_WIDTH + 8}/>
            </div>
          </NavLink>
          <Tooltip title='overview' aria-label='overview' placement='right'>
            <NavLink activeClassName='l-active' className='sidebar-link' to={Routes.OVERVIEW} exact>
              <div className='sidebar-link-content-wrapper'>
                <img alt='' className='sidebar-link-icon' src={homeIcon} width={SIDEBAR_ICON_WIDTH}/>
              </div>
            </NavLink>
          </Tooltip>
          <Tooltip title='launch' aria-label='launch' placement='right'>
            <NavLink activeClassName='l-active' className='sidebar-link' to={Routes.LAUNCH} exact>
              <div className='sidebar-link-content-wrapper'>
                <img alt='' className='sidebar-link-icon' src={launchIcon} width={SIDEBAR_ICON_WIDTH}/>
              </div>
            </NavLink>
          </Tooltip>
          <Tooltip title='demographics' aria-label='demographics' placement='right'>
            <NavLink activeClassName='l-active' className='sidebar-link' to={Routes.DEMOGRAPHICS} exact>
              <div className='sidebar-link-content-wrapper'>
                <img alt='' className='sidebar-link-icon' src={demographicsIcon} width={SIDEBAR_ICON_WIDTH}/>
              </div>
            </NavLink>
          </Tooltip>
          <Tooltip title='campaigns' aria-label='campaigns' placement='right'>
            <NavLink activeClassName='l-active' className='sidebar-link' to={Routes.CAMPAIGNS} exact>
              <div className='sidebar-link-content-wrapper'>
                <img alt='' className='sidebar-link-icon' src={campaignsIcon} width={SIDEBAR_ICON_WIDTH}/>
              </div>
            </NavLink>
          </Tooltip>
        </div>
        <div className='sidebar-content sidebar-content-bottom'>
          <Tooltip title='help' aria-label='help' placement='right'>
            <NavLink activeClassName='l-active' className='sidebar-link' to={Routes.HELP} exact>
              <div className='sidebar-link-content-wrapper'>
                <img alt='' className='sidebar-link-icon' src={helpIcon} width={SIDEBAR_ICON_WIDTH}/>
              </div>
            </NavLink>
          </Tooltip>
          <Tooltip title='profile' aria-label='profile' placement='right'>
            <NavLink className='sidebar-link' to={Routes.SETTINGS}
              onClick={() => onSetDefaultSettingsPage(SettingsTabs.PROFILE_TAB)} exact>
              <div className='sidebar-link-content-wrapper'>
                <Avatar facebookId={ facebookUser !== null ? facebookUser.id : null } size={ SIDEBAR_ICON_WIDTH + 8 } round />
              </div>
            </NavLink>
          </Tooltip>
          <Tooltip title='settings' aria-label='settings' placement='right'>
            <NavLink activeClassName='l-active' className='sidebar-link' to={Routes.SETTINGS} exact>
              <div className='sidebar-link-content-wrapper'>
                <img alt='' className='sidebar-link-icon' src={settingsIcon} width={SIDEBAR_ICON_WIDTH}/>
              </div>
            </NavLink>
          </Tooltip>
        </div>
      </div>
    );
  }
}

const Standalone = Sidebar;
export { Standalone };

const mapStateToProps = state => ({
  facebookUser: state.facebookUserState.facebookUser,
});

const mapDispatchToProps = dispatch => ({
  onSetDefaultSettingsPage: defaultPage =>
    dispatch({
      type: Actions.SETTINGS_DEFAULT_PAGE_SET,
      defaultPage,
    }),
});

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(Sidebar);
