// ---------- DEPENDENCIES ---------- //

import React, {
  Component,
} from 'react';
import { compose, } from 'recompose';
import { connect } from 'react-redux';

// ---------- LOCAL DEPENDENCIES ---------- //

// CSS.

import './Settings.css';

// Constants.

import * as SettingsTabs from '../../../constants/SettingsTabs';
import * as Actions from '../../../constants/ActionTypes';

// Components.

import withAuthorization from '../../Providers/withAuthorization';
import PlatformsSettings from '../../Pages/PlatformsSettings';
import ProfileSettings from '../../Pages/ProfileSettings';
import SettingsSidebar from '../../SettingsSidebar';

// ---------- MAIN ---------- //

const INIT_STATE = {
  selectedTab: SettingsTabs.PLATFORMS_TAB,
};

// ---------- MAIN ---------- //

class Settings extends Component {
  constructor(props) {
    super(props);

    this.state = INIT_STATE;
  }

  handlePageChange = (tabID) => {
    this.setState({
      selectedTab: tabID,
    });
  }

  componentDidMount = () => {
    const {
      defaultTab,
      onSetDefaultSettingsPage,
    } = this.props;
    if (defaultTab !== null && defaultTab !== undefined) {
      this.setState({
        selectedTab: defaultTab
      });
      onSetDefaultSettingsPage(null);
    }
  }

  render() {
    const { selectedTab } = this.state;

    return (
      <div className="settings-grid">
        <div className="settings-grid-settings-sidebar">
          <SettingsSidebar selectedTab={selectedTab} onChange={this.handlePageChange} />
        </div>
        <div className="settings-grid-settings-content">
          {selectedTab === SettingsTabs.PLATFORMS_TAB && <PlatformsSettings onNav={this.handlePageChange} />}
          {selectedTab === SettingsTabs.PROFILE_TAB && <ProfileSettings onNav={this.handlePageChange} />}
        </div>
      </div>
    );
  }
}

const authCondition = (authUser) => !!authUser;

const Standalone = Settings;
export {
  Standalone,
};

const mapStateToProps = state => ({
  defaultTab: state.settingsState.defaultPage,
});

const mapDispatchToProps = dispatch => ({
  onSetDefaultSettingsPage: defaultPage =>
    dispatch({
      type: Actions.SETTINGS_DEFAULT_PAGE_SET,
      defaultPage,
    }),
});

export default compose(
  withAuthorization(authCondition),
  connect(mapStateToProps, mapDispatchToProps),
)(Settings);