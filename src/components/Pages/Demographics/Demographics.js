// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';
import { compose } from 'recompose';
import {
  withStyles,
} from '@material-ui/core';

// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './Demographics.css';

// Components.

import withAuthorization from '../../Providers/withAuthorization';
import DemographicControl from '../../DemographicControl';
import Fingerprint from '../../Fingerprint';

// --------------- CONSTANTS --------------- //

const demographicStyles = theme => ({
  demographicRoot: {
    height: '100vh',
    width : '100%',
  },
  demographicControlContainer: {
    position: 'absolute',
    height  : '100vh',
    width   : 250,
    zIndex  : 3000,
    right   : 0,
    top     : 0,
  },
  demographicContent: {
    height: '100vh',
    width : '100%',
  }
});

// --------------- MAIN --------------- //

class Demographics extends Component {
  render() {
    const { classes } = this.props;

    return (
      <div className={classes.demographicRoot}>
        <div className={classes.demographicContent}>
          {<Fingerprint />}
        </div>
        <div className={classes.demographicControlContainer}>
          <DemographicControl/>
        </div>
      </div>
    );
  }
}

const authCondition = (authUser) => !!authUser;

export default compose(
  withAuthorization(authCondition),
  withStyles(demographicStyles),
)(Demographics);
