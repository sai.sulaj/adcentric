// ---------- DEPENDENCIES ---------- //

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import {
  Button,
  withStyles,
} from '@material-ui/core';

// ---------- LOCAL DEPENDENCIES ---------- //

// Constants.

import * as Actions from '../../../constants/ActionTypes';
import * as ErrorCodesEnum from '../../../constants/ErrorCodesEnum';
import * as PlatformIDs from '../../../constants/PlatformIDs';

// CSS.

import './PlatformsSettings.css';

// Components.

import withAuthorization from '../../Providers/withAuthorization';
import FacebookAuth from '../../FacebookAuth';
import facebookLoginButton from '../../FacebookLoginButton';
import { Db } from '../../../firebase';
import ConnectAdAccountDialog from '../../ConnectAdAccountDialog';

// APIs.

import { FacebookAPI } from '../../../facebook';

// ---------- CONSTANTS ---------- //

const materialStyles = {
  connectAccountButton: {},
};

const INIT_STATE = {
  connectAdAccountDialogOpen: false,
  facebookAdAccounts        : [],
  selectedAdAccountID       : null,
  hasFacebookAdAccounts     : true,
};

// ---------- MAIN ---------- //

class Settings extends Component {
  constructor(props) {
    super(props);

    this.state = INIT_STATE;
  }

  componentDidMount = () => {
    this.fetchFacebookData();
  }

  fetchFacebookData = (user) => {
    const {
      facebookAPI,
      facebookUser,
      platformsAuthState,
      onSetPlatformAuthError,
    } = this.props;

    if (facebookAPI.available && ((facebookUser !== null && facebookUser !== undefined) || user !== undefined)) {
      const facebookID = user === undefined ? facebookUser.id : user.userID;
      FacebookAPI.fetchAdAccounts(facebookAPI.api, facebookID).then((resp) => {
        this.setState({
          facebookAdAccounts   : resp.data,
          hasFacebookAdAccounts: resp.data.length !== 0,
        });
      }).catch((err) => {
        console.log(err);
          if (err.code === ErrorCodesEnum.FACEBOOK_O_AUTH_ERROR
              || err.code === ErrorCodesEnum.FACEBOOK_NO_ACCESS_CODE) {
            onSetPlatformAuthError(PlatformIDs.FACEBOOK, ErrorCodesEnum.FACEBOOK_O_AUTH_ERROR);
          } else {
            // TODO: Handle.
          }
      });
    } else if (platformsAuthState[PlatformIDs.FACEBOOK]) {
      onSetPlatformAuthError(PlatformIDs.FACEBOOK, ErrorCodesEnum.FACEBOOK_O_AUTH_ERROR);
    }
  }
  
  /**
   * Facebook authorization success callback. Updates redux
   * State with user's Facebook data.
   *
   * @param {JSON} user -> Facebook user data object.
   * @param {JSON} resp -> Facebook API response object.
   */
  onFBLoginSuccess = (user, resp) => {
    const {
      onSetFacebookUser,
      authUser,
      onUpdateFacebookAPIAccessToken,
    } = this.props;

    onSetFacebookUser(user);
    onUpdateFacebookAPIAccessToken(user.accessToken);
    Db.doUpdateFacebookUser(authUser.uid, user).then(() => { this.fetchFacebookData(user) });
  };

  /**
   * Facebook authorization error callback.
   *
   * @param {JSON} err -> Error object.
   */
  onFBLoginError = (err) => {
    // TODO.
    console.error(err);
  };

  onConnectFacebookAccount = () => {
    this.setState({
      connectAdAccountDialogOpen: true,
    });
  }

  handleAccountDialogClose = () => {
    this.setState({
      connectAdAccountDialogOpen: false,
    });
  }

  handleAdAccountSelect = (adAccountID) => {
    const {
      onSetFacebookAPISelectedAdAccountID,
      authUser,
    } = this.props;

    this.setState({
      selectedAdAccountID       : adAccountID,
      connectAdAccountDialogOpen: false,
    });

    Db.doSetSelectedFacebookAdAccountID(authUser.uid, adAccountID);
    onSetFacebookAPISelectedAdAccountID(adAccountID);
  }

  render() {
    const {
      connectAdAccountDialogOpen,
      facebookAdAccounts,
      hasFacebookAdAccounts,
    } = this.state;
    const {
      facebookAPI,
      facebookUser,
    } = this.props;

    if (facebookAPI.available
        && facebookUser !== null
        && facebookUser !== undefined
        && facebookAdAccounts.length === 0
        && hasFacebookAdAccounts) {
      this.fetchFacebookData();
    }

    return (
      <div className='platforms-settings-root'>
        <h2 className='overview-header'>PLATFORMS SETTINGS</h2>
        <FacebookAuth
          callback  = {this.onFBLoginSuccess}
          trigger   = {facebookLoginButton}
          onFailure = {this.onFBLoginError}
        />
        <Button
          onClick={() => this.onConnectFacebookAccount()}>Connect Facebook Account</Button>
        <ConnectAdAccountDialog onClose={this.handleAccountDialogClose} onSelect={this.handleAdAccountSelect}
          accounts={facebookAdAccounts.map(a => `Account ID ${a.account_id}`)} open={connectAdAccountDialogOpen} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  authUser          : state.sessionState.authUser,
  facebookAPI       : state.facebookAPIState,
  facebookUser      : state.facebookUserState.facebookUser,
  platformsAuthState: state.platformsAuthState,
});

const mapDispatchToProps = dispatch => ({
  onSetFacebookUser: facebookUser =>
    dispatch({
      type        : Actions.FACEBOOK_USER_SET,
      facebookUser: facebookUser,
    }),
  onSetFacebookAPI: facebookAPIState =>
    dispatch({
      type: Actions.FACEBOOK_API_SET,
      ...facebookAPIState,
    }),
  onUpdateFacebookAPIAccessToken: accessToken =>
    dispatch({
      type          : Actions.FACEBOOK_API_ACCESS_TOKEN_UPDATE,
      newAccessToken: accessToken,
    }),
  onSetFacebookAPISelectedAdAccountID: adAccountID =>
    dispatch({
      type               : Actions.FACEBOOK_API_SELECTED_AD_ACCOUNT_ID,
      selectedAdAccountID: adAccountID,
    }),
  onSetPlatformAuthError: (platformID, errorCode) =>
    dispatch({
      type         : Actions.PLATFORM_AUTH_ERROR_SET,
      authErrorID  : platformID,
      authErrorCode: errorCode,
    }),
});

const authCondition = authUser => !!authUser;

const Standalone = Settings;
export { Standalone };

export default compose(
  withAuthorization(authCondition),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withRouter,
  withStyles(materialStyles),
)(Settings);
