// ------------- DEPENDENCIES ------------- //

import React, { Component } from 'react';
import {
  withRouter,
} from 'react-router-dom';
import {
  compose,
} from 'recompose';
import {
  Grid,
  Row,
} from 'react-bootstrap';

// ------------- LOCAL DEPENDENCIES ------------- //

// CSS.
import './SignUp.css';

// Components.

import SignUpForm from '../../SignUpForm';

// ------------- MAIN ------------- //

class SignUp extends Component {
  render() {
    return (
      <Grid
        className='signup-root'>
        <Row>
          <div
            className='panel-heading'>
            <div
              className = 'panel-title text-center'>
              <h1
                className='title'>
                Adcentric
              </h1>
              <hr />
            </div>
          </div>
          <div
            className='signup-main center-signup'>
            <SignUpForm />
          </div>
        </Row>
      </Grid>
    );
  }
}

const Standalone = SignUp;
export {
  Standalone,
};

export default compose(
  withRouter,
)(SignUp);