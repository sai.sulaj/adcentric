// ------------- DEPENDENCIES ------------- //

import React, { Component } from 'react';
import {
  withRouter,
} from 'react-router-dom';
import {
  compose,
} from 'recompose';
import {
  Grid,
  Row,
} from 'react-bootstrap';

// ------------- LOCAL DEPENDENCIES ------------- //

// CSS.
import './SignIn.css';

// Components.

import SignInForm from '../../SignInForm';

// ------------- MAIN ------------- //

class SignIn extends Component {
  render() {
    return (
      <Grid
        className='signin-root'>
        <Row>
          <div
            className='panel-heading'>
            <div
              className = 'panel-title text-center'>
              <h1
                className='title'>
                Adcentric
              </h1>
              <hr />
            </div>
          </div>
          <div
            className='signin-main center-signin'>
            <SignInForm />
          </div>
        </Row>
      </Grid>
    );
  }
}

const Standalone = SignIn;
export {
  Standalone,
};

export default compose(
  withRouter,
)(SignIn);