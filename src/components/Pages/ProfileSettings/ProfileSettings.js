// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';
import { compose } from 'recompose';
import {
  TrendingUpRounded,
  HourglassFullRounded,
} from '@material-ui/icons';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Moment from 'moment';

// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './ProfileSettings.css';

// Components,

import {
  ProfileMainComponent,
  SimpleProfileDataComponent,
} from '../../ProfileSettingsComponents';
import PlatformsStatusTabs from '../../PlatformsStatusTabs';
import { Db, } from '../../../firebase';

// Constants.

import * as DatabaseFields from '../../../constants/DatabaseFields';

// --------------- CONSTANTS --------------- //

const INIT_STATE = {
  hoursLogged: 0,
};

// --------------- MAIN --------------- //

class ProfileSettings extends Component {
  constructor(props) {
    super(props)

    this.state = INIT_STATE;
  }

  componentDidMount = () => {
    const { authUser } = this.props;
    Db.doFetchUser(authUser.uid).then((doc) => {
      if (doc.exists) {
        const userData = doc.data();

        if (userData[DatabaseFields.FIRST_LOGIN] !== undefined) {
          const firstLogin = Moment(userData[DatabaseFields.FIRST_LOGIN]);

          if (firstLogin.isValid()) {
            const hoursLogged = Moment().diff(firstLogin, 'hours');
            this.setState({ hoursLogged, });
          }
        }
      }
    });
  }
  
  doNav = (tabID) => {
    const { onNav } = this.props;
    onNav(tabID);
  }

  render() {
    const {
      hoursLogged,
    } = this.state;

    return (
      <div className="profile-root">
        <div className="profile-profile-content">
          <div className="profile-profile-content-row-1-col-1">
            <h2 className='profile-settings-header'>PROFILE</h2>
          </div>
          <div className="profile-profile-content-row-2-col-1">
            <ProfileMainComponent location={'Vancouver BC, Canada.'} />
          </div>
          <div className="profile-profile-content-row-3-col-1">
            <h3 className='platform-settings-link-header'>PLATFORM INTEGRATION STATUS</h3>
          </div>
          <div className="profile-profile-content-row-4-col-1">
            <PlatformsStatusTabs onNav={this.doNav} />
          </div>
          <div className="profile-profile-content-row-5-col-1">
            <h3 className='profile-settings-stats-header'>STATS</h3>
          </div>
          <div className="profile-profile-content-row-6-col-1">
            <SimpleProfileDataComponent icon={<HourglassFullRounded />} label='Hours logged' value={hoursLogged} />
          </div>
          <div className="profile-profile-content-row-6-col-2">
            <SimpleProfileDataComponent icon={<TrendingUpRounded />} label='Avg. CTR gain' value={0.17} percentage/>
          </div>
          <div className="profile-profile-content-row-7-col-1"></div>
        </div>
        <div className="profile-activity-content"></div>
      </div>
    );
  }
}

ProfileSettings.propTypes = {
  onNav: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  authUser: state.sessionState.authUser,
});

export default compose(
  connect(mapStateToProps, null),
)(ProfileSettings);
