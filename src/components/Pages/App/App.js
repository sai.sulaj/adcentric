// ---------- DEPENDENCIES ---------- //

import React from 'react';
import {
  Route,
  BrowserRouter as Router,
} from 'react-router-dom';
import { compose } from 'recompose';
import { connect } from 'react-redux';

// ---------- LOCAL DEPENDENCIES ---------- //

// Constants.

import * as Routes from '../../../constants/Routes';
import * as PlatformIDs from '../../../constants/PlatformIDs';

// Pages.

import Overview from '../Overview';
import Launch from '../Launch';
import SignIn from '../SignIn';
import SignUp from '../SignUp';
import Settings from '../Settings';
import Campaigns from '../Campaigns';
import Demographics from '../Demographics';
import Help from '../Help';

// Components.

import withAuthentication from '../../Providers/withAuthentication';
import withAuthorization from '../../Providers/withAuthorization';
import Sidebar from '../../Sidebar';
import WithPlatforms from '../../Providers/WithPlatforms';
import PlatformAuthDialog from '../../PlatformAuthDialog';

// ---------- MAIN ---------- //

const ConsoleApp = ({platformAuthErrorState, platformAuthErrorMessage}) => (
  <div className='sidebar-content-grid-container'>
    <div className='grid-sidebar-container'>
      <Sidebar />
    </div>
    <div className='grid-content-container'>
      <Route
        path      = { Routes.OVERVIEW }
        component = { Overview }
        exact />

      <Route
        path      = { Routes.ROOT }
        component = { Overview }
        exact />
      
      <Route
        path      = { Routes.LAUNCH }
        component = { Launch }
        exact />

      <Route
        path      = { Routes.SETTINGS }
        component = { Settings }
        exact />

      <Route
        path      = { Routes.DEMOGRAPHICS }
        component = { Demographics }
        exact />

      <Route
        path      = { Routes.CAMPAIGNS }
        component = { Campaigns }
        exact />

      <Route
        path      = { Routes.HELP }
        component = { Help }
        exact />

    </div>
    <PlatformAuthDialog open={PlatformIDs.NONE !== platformAuthErrorState} message={platformAuthErrorMessage} />
  </div>
);

const authCondition = (authUser) => !!authUser;

const mapConsoleStateToProps = state => ({
  platformAuthErrorState: state.platformAuthErrorState.authErrorID,
  platformAuthErrorMessage: state.platformAuthErrorState.authErrorMessage,
});

const WrappedConsoleApp = compose(
  withAuthorization(authCondition),
)(ConsoleApp);

const App = ({platformAuthErrorState, platformAuthErrorMessage}) => (
  <Router
    className='app-root full-width'>
    <div>
      <WrappedConsoleApp platformAuthErrorState={platformAuthErrorState} platformAuthErrorMessage={platformAuthErrorMessage} />

      <Route
        path      = { Routes.SIGN_IN }
        component = { SignIn }
        exact />

      <Route
        path      = { Routes.SIGN_UP }
        component = { SignUp }
        exact />
    </div>
  </Router>
);

const Standalone = App;
export {
  Standalone,
};

export default compose(
  withAuthentication,
  WithPlatforms,
  connect(mapConsoleStateToProps, null),
)(App);