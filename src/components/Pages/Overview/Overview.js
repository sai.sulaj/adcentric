// ---------- DEPENDENCIES ---------- //

import React, { Component } from 'react';
import {
  connect,
} from 'react-redux';
import {
  compose,
} from 'recompose';
import {
  withRouter,
} from 'react-router-dom';
import _ from 'lodash';
import Dimensions from 'react-dimensions';

// ---------- LOCAL DEPENDENCIES ---------- //

// Components.

import withAuthorization from '../../Providers/withAuthorization';
import CustomAreaChart from '../../CustomAreaChart';
import PlatformsTabs from '../../PlatformsTabs';
import SimpleCampaignsList from '../../SimpleCampaignsList';
import SimpleDemographicsOverview from '../../SimpleDemographicsOverview';

// CSS.

import './Overview.css';

// Constants.

import * as PlatformIDs from '../../../constants/PlatformIDs';

// ---------- CONSTANTS ---------- //

const INIT_STATE = {
  graphData       : null,
  selectedPlatform: null,
  campaignsData   : null,
};

// ---------- MAIN ---------- //

class Overview extends Component {
  constructor(props) {
    super(props);

    this.state = INIT_STATE;
  }

  componentDidMount = () => {
    this.setState({
      graphData       : this.getGraphData(),
      selectedPlatform: PlatformIDs.FACEBOOK,
      campaignsData   : this.getCampaignsData(),
    });
    // TODO: Remove.
    this.setStateInterval = window.setInterval(() => {
      this.setState({
        graphData    : this.getGraphData(),
        campaignsData: this.getCampaignsData(),
      });
    }, 2000);
  }

  // TODO: Fetch real data.
  getCampaignsData = () => {
    return _.range(20).map((ph, i) => {
      return { number: i + 1 };
    })
  }

  // TODO: Fetch real data.
  getGraphData = () => {
    return _.range(7).map(() => {
      return [
        { x: 1, y: _.random(0, 10)},
        { x: 2, y: _.random(0, 10)},
        { x: 3, y: _.random(0, 10)},
        { x: 4, y: _.random(0, 10)},
        { x: 5, y: _.random(0, 10)},
        { x: 6, y: _.random(0, 10)},
        { x: 7, y: _.random(0, 10)},
      ];
    });
  }

  onPlatformSelect = (platformName) => {
    this.setState({
      selectedPlatform: platformName,
    });
  }

  render() {
    const {
      graphData,
      campaignsData,
    } = this.state;
    const {
      containerHeight,
      containerWidth,
    } = this.props;

    return (
      <div className='overview-root'>
        <div className="overview-row-1-col-1">
          <div className="overview-row-1-col-1-heading">
            <h2 className='overview-header'>OVERVIEW</h2>
          </div>
          <div className="overview-row-1-col-1-subheading">
            <PlatformsTabs onPlatformSelect={this.onPlatformSelect} />
          </div>
          <div className="overview-row-1-col-1-content">
            <CustomAreaChart data={ graphData } height={ containerHeight * 0.7 } width={ containerWidth * 1.5 } />
          </div>
        </div>
        <div className="overview-row-1-col-2">
          <div className='overview-campaigns-list-wrapper'>
            {<SimpleCampaignsList campaigns={campaignsData} />}
          </div>
        </div>
        <div className="overview-row-2">
          <SimpleDemographicsOverview />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  authUser: state.sessionState.authUser,
});

const authCondition = (authUser) => !!authUser;

const Standalone = Overview;
export {
  Standalone,
};

export default compose(
  withAuthorization(authCondition),
  connect(mapStateToProps),
  withRouter,
  Dimensions(),
)(Overview);