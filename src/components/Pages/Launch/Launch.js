// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';
import Moment from 'moment';
import Pose from 'react-pose';
import { compose } from 'recompose';
import { connect } from 'react-redux';

// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './Launch.css';

// Components.

import LaunchStepper from '../../LaunchStepper';
import LaunchCreateForm from '../../LaunchCreateForm';
import LaunchSetBudgetForm from '../../LaunchSetBudgetForm';
import LaunchCreativesForm from '../../LaunchCreativesForm';

// APIs.

import { FacebookAPI } from '../../../facebook';

// Constants.

import * as ErrorCodesEnum from '../../../constants/ErrorCodesEnum';
import * as PlatformIDs from '../../../constants/PlatformIDs';
import * as Actions from '../../../constants/ActionTypes';

// --------------- CONSTANTS --------------- //

const INIT_STATE = {
  step         : 0,
  createForm   : {},
  budgetForm   : {},
  creativesForm: {},
};

export const FORM_TYPE_ENUM = {
  CREATE   : 'createForm',
  BUDGET   : 'budgetForm',
  CREATIVES: 'creativesForm',
};

const REQUIRED_FIELDS = new Map([
  [0, {
    campaignNameInput            : (val) => val !== undefined && val !== '',
    campaignTargetWebsiteURLInput: (val) => val !== undefined && val !== '', // TODO: Reliably test URL.
  }],
  [1, {
    campaignBudgetType : (val) => val !== undefined && val !== '',
    campaignBudgetValue: (val) => val !== undefined && val > 0,  // HTML input type should ensure is number.
    campaignEndDate    : (date) => date instanceof Moment,
    campaignStartDate  : (date) => date instanceof Moment,
  }],
  [2, {
    campaignAdHeadlineInput: (val) => val !== undefined && val !== '',
    campaignAdTextInput    : (val) => val !== undefined && val !== '',
    campaignImageFilesInput: (files) => files instanceof Array && files.length > 0,
  }],
]);

const AccordionButton = Pose.button({
  visible: { height: 45, paddingLeft: 10, paddingRight: 10, paddingTop: 10, paddingBottom: 10 },
  hidden : { height: 0, padding: 0 },
});

// --------------- MAIN --------------- //

class Launch extends Component {
  constructor(props) {
    super(props);

    this.state = INIT_STATE;
  }

  componentDidMount = () => {
    setTimeout(() => {
      const {
        onSetPlatformAuthError,
        facebookAPI,
        platformsAuthState,
      } = this.props;

      if (platformsAuthState.facebook !== true
        || !facebookAPI.available
        || facebookAPI.selectedAdAccountID == null
        || facebookAPI.selectedAdAccountID === ''
        || facebookAPI.api == null) {
        onSetPlatformAuthError(PlatformIDs.FACEBOOK, ErrorCodesEnum.FACEBOOK_NO_ACCESS_CODE);
      }
    }, 1000);
  }
  
  handleFormChange = (formType, name, value) => {
    this.setState(prevState => {
      const newState = {
        ...prevState,
        [formType]: {
          ...prevState[formType],
          [name]: value,
        },
      }
      newState.step = this.calculateStepperStep(newState);
      return newState;
    });
  }

  calculateStepperStep = (state) => {
    const {
      createForm,
      budgetForm,
      creativesForm,
    } = state;
    const allFields = {
      ...createForm,
      ...budgetForm,
      ...creativesForm,
    };

    for (let [step, fields] of REQUIRED_FIELDS.entries()) {
      for (let [field, validate] of Object.entries(fields)) {
        if (!validate(allFields[field])) return step;
      }
    }
    return REQUIRED_FIELDS.size;
  }

  handleLaunch = () => {
    const {
      createForm,
      budgetForm,
      creativesForm,
    } = this.state;
    const {
      facebookAPI,
      platformsAuthState,
    } = this.props;

    if (platformsAuthState.facebook === true
      && facebookAPI.available
      && facebookAPI.selectedAdAccountID != null
      && facebookAPI.selectedAdAccountID !== ''
      && facebookAPI.api != null) {
      FacebookAPI.launchCampaign(facebookAPI.api, facebookAPI.selectedAdAccountID, {
        name                           : createForm.campaignNameInput,
        [budgetForm.campaignBudgetType]: parseFloat(budgetForm.campaignBudgetValue) * 100,
        start_time                     : budgetForm.campaignStartDate.toISOString(),
        end_time                       : budgetForm.campaignEndDate.toISOString(),
      });
    }
  }

  render() {
    const {
      step,
      createForm,
      budgetForm,
      creativesForm,
    } = this.state;

    return (
      <div className='launch-root'>
        <div className='launch-content-wrapper'>
          <h2 className='launch-header'>LAUNCH</h2>
          <LaunchCreateForm onFormChange={this.handleFormChange} {...createForm} />
          <LaunchSetBudgetForm onFormChange={this.handleFormChange} {...budgetForm} />
          <LaunchCreativesForm onFormChange={this.handleFormChange} {...creativesForm} />
        </div>
        <div className='launch-stepper-wrapper'>
          <LaunchStepper activeStep={step} />
          <div className='launch-stepper-launch-wrapper'>
            <AccordionButton
              disabled={step !== REQUIRED_FIELDS.size}
              pose={step === REQUIRED_FIELDS.size ? 'visible' : 'hidden'}
              onClick={() => this.handleLaunch()}
              className='launch-launch-button'>LAUNCH</AccordionButton>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  authUser          : state.sessionState.authUser,
  facebookAPI       : state.facebookAPIState,
  facebookUser      : state.facebookUserState.facebookUser,
  platformsAuthState: state.platformsAuthState,
});

const mapDispatchToProps = dispatch => ({
  onSetFacebookUser: facebookUser =>
    dispatch({
      type        : Actions.FACEBOOK_USER_SET,
      facebookUser: facebookUser,
    }),
  onSetFacebookAPI: facebookAPIState =>
    dispatch({
      type: Actions.FACEBOOK_API_SET,
      ...facebookAPIState,
    }),
  onUpdateFacebookAPIAccessToken: accessToken =>
    dispatch({
      type          : Actions.FACEBOOK_API_ACCESS_TOKEN_UPDATE,
      newAccessToken: accessToken,
    }),
  onSetFacebookAPISelectedAdAccountID: adAccountID =>
    dispatch({
      type               : Actions.FACEBOOK_API_SELECTED_AD_ACCOUNT_ID,
      selectedAdAccountID: adAccountID,
    }),
  onSetPlatformAuthError: (platformID, errorCode) =>
    dispatch({
      type         : Actions.PLATFORM_AUTH_ERROR_SET,
      authErrorID  : platformID,
      authErrorCode: errorCode,
    }),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps)
)(Launch);
