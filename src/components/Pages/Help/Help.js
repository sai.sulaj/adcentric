// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';
import {
  IconButton,
  InputBase,
  withStyles,
} from '@material-ui/core';
import {
  SearchRounded,
} from '@material-ui/icons';
import TS from 'full-text-search-light';

// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './Help.css';

// Components.

import { Db } from '../../../firebase';
import Faq from '../../Faq';
import ContactUs from '../../ContactUs';

// Constants.

import * as DatabaseFields from '../../../constants/DatabaseFields';

// --------------- CONSTANTS --------------- //

const materialStyles = {
  searchRoot : {
    position       : 'fixed',
    right          : 20,
    top            : 20,
    padding        : '2px 6px 2px 8px',
    display        : 'flex',
    alignItems     : 'center',
    width          : 300,
    backgroundColor: '#FFFFFF',
    boxShadow      : '0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)',
    transition     : 'all 0.3s cubic-bezier(.25,.8,.25,1)',
    borderRadius   : 40,
    '&:hover'      : {
      boxShadow: '0 2px 6px rgba(0,0,0,0.25), 0 2px 4px rgba(0,0,0,0.22)',
    },
    zIndex: 300,
  },
  searchField: {
    marginLeft: 8,
    flex      : 1,
    fontSize  : 16,
  },
  searchIcon : {
    padding: 3,
  },
  contentHeader: {
    margin    : '15px 5px 5px 10px',
    fontWeight: 300,
  },
};

const INIT_STATE = {
  searchValue: '',
  faqContent: [],
  faqContentOpenState: [],
};

// --------------- MAIN --------------- //

class Help extends Component {
  constructor(props) {
    super(props);

    this.state = INIT_STATE;

    this.textSearch = new TS({ ignore_case: true });
  }

  componentDidMount = () => {
    Db.doFetchFaq().then((data) => {
      this.setState({
        faqContent         : data,
        faqContentOpenState: data.sort((a, b) => a[DatabaseFields.FAQ_INDEX] - b[DatabaseFields.FAQ_INDEX]).map(d => false),
      });

      for (let i = 0; i < data.length; i++) {
        this.textSearch.add(data[i], (key, val) => key !== 'title');
      }
    });
  }
  
  toggleFAQContentOpen = (index) => {
    this.setState(prevState => {
      const    prevOpen = prevState.faqContentOpenState;
      prevOpen[index]   = !prevOpen[index];
      return {
        faqContentOpenState: prevOpen,
      };
    });
  }

  searchFAQText = () => {
    const {
      searchValue,
      faqContentOpenState,
    } = this.state;

    const results = this.textSearch.search(searchValue).map(r => r.index);

    this.setState({
      faqContentOpenState: (Array(faqContentOpenState.length).fill(0)).map((a, i) => results.includes(i)),
    });
  }

  render() {
    const { classes } = this.props;
    const {
      searchValue,
      faqContent,
      faqContentOpenState,
    } = this.state;

    return (
      <div className='help-root'>
        <div className={classes.searchRoot} elevation={1}>
          <InputBase value={searchValue} className={classes.searchField}
            placeholder='Search' onChange={(evt) => this.setState({ searchValue: evt.target.value, })}
            onKeyPress={(ev) => {if (ev.key === 'Enter') this.searchFAQText()}}/>
          <IconButton onClick={() => this.searchFAQText()} className={classes.searchIcon}>
            <SearchRounded />
          </IconButton>
        </div>
        <div className='help-header'>
          <h2 className='help-header'>HELP</h2>
        </div>
        <div className='help-content'>
          <h3 className={classes.contentHeader}>FAQ</h3>
          <Faq faqContent={faqContent} faqContentOpenState={faqContentOpenState}
              onContentOpenStateUpdate={this.toggleFAQContentOpen} searchValue={searchValue} />
          <ContactUs />
        </div>
      </div>
    );
  }
}

export default withStyles(materialStyles)(Help);
