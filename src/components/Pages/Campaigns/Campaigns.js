// ---------- DEPENDENCIES ---------- //

import React, { Component } from 'react';
import {
  connect,
} from 'react-redux';
import {
  compose,
} from 'recompose';
import {
  withRouter,
} from 'react-router-dom';
import _ from 'lodash';
import Dimensions from 'react-dimensions';

// ---------- LOCAL DEPENDENCIES ---------- //

// Components.

import withAuthorization from '../../Providers/withAuthorization';
import CustomAreaChart from '../../CustomAreaChart';
import PlatformsTabs from '../../PlatformsTabs';
import MetricsSelector from '../../MetricsSelector';
import DetailedCampaignsList from '../../DetailedCampaignsList';

// CSS.

import './Campaigns.css';

// ---------- CONSTANTS ---------- //

const INIT_STATE = {
  graphData          : null,
  selectedPlatform   : null,
  campaignsData      : null,
  selectedCampaignIDs: [],
};

// ---------- MAIN ---------- //

const dummyCampaignMetrics = [
  'IMPRESSIONS',
  'CLICKS',
  'SPENT',
  'CONVERSIONS'
];

const dummyCampaignsColumns = [
  {
    width   : 200,
    flexGrow: 1.0,
    label   : 'Name',
    dataKey : 'name',
  },
  {
    width   : 200,
    flexGrow: 1.0,
    label   : 'Status',
    dataKey : 'status',
  },
  {
    width   : 200,
    flexGrow: 1.0,
    label   : 'Start',
    dataKey : 'start',
  },
  {
    width   : 200,
    flexGrow: 1.0,
    label   : 'End',
    dataKey : 'end',
  },
  {
    width   : 200,
    flexGrow: 1.0,
    label   : 'Budget',
    dataKey : 'budget',
  },
  {
    width   : 200,
    flexGrow: 1.0,
    label   : 'Ads',
    dataKey : 'ads',
  },
  {
    width   : 200,
    flexGrow: 1.0,
    label   : 'IMPR.',
    dataKey : 'impressions',
  },
  {
    width   : 200,
    flexGrow: 1.0,
    label   : 'Click',
    dataKey : 'clicks',
  },
  {
    width   : 200,
    flexGrow: 1.0,
    label   : 'Spent',
    dataKey : 'spent',
  },
  {
    width   : 200,
    flexGrow: 1.0,
    label   : 'Conv.',
    dataKey : 'conversions',
  },
];

class Campaigns extends Component {
  constructor(props) {
    super(props);

    this.state = INIT_STATE;
  }

  componentDidMount = () => {
    this.setState({
      graphData       : this.getGraphData(),
      selectedPlatform: 'facebook',
      campaignsData   : this.getCampaignsData(),
    });
  }

  // TODO: Fetch real data.
  getCampaignsData = () => {
    return _.range(120).map((ph, i) => {
      const start  = Math.round(((Math.random() / 2) + 0.5) * i * 1000);
      const budget = Math.round(Math.random() * 10000);
      return {
        id    : i,
        name  : `Campaign ${i}`,
        status: Math.random() > 0.5 ? 'ACTIVE': 'INACTIVE',
        start      : start,
        end        : start + 5000,
        budget     : budget,
        ads        : 'AYYY',
        impressions: Math.round(Math.random() * 20000),
        clicks     : Math.round(Math.random() * 100000),
        spent      : Math.round(budget / 2),
        conversions: Math.round(Math.random() * 5000),
      };
    })
  }

  // TODO: Fetch real data.
  getGraphData = () => {
    return _.range(7).map(() => {
      return [
        { x: 1, y: _.random(0, 10)},
        { x: 2, y: _.random(0, 10)},
        { x: 3, y: _.random(0, 10)},
        { x: 4, y: _.random(0, 10)},
        { x: 5, y: _.random(0, 10)},
        { x: 6, y: _.random(0, 10)},
        { x: 7, y: _.random(0, 10)},
      ];
    });
  }

  onPlatformSelect = (platformName) => {
    this.setState({
      selectedPlatform: platformName,
    });
  }

  onSelectedCampaignsUpdate = (updatedCampaignIDs) => {
    this.setState({
      selectedCampaignIDs: updatedCampaignIDs,
    });
  }

  render() {
    const {
      graphData,
      campaignsData,
    } = this.state;
    const {
      containerHeight,
      containerWidth,
    } = this.props;

    return (
      <div className="campaigns-root">
        <div className="campaigns-row-1-col-1">
          <div className="campaigns-row-1-col-1-heading">
            <h2 className='campaigns-header'>CAMPAIGNS</h2>
          </div>
          <div className="campaigns-row-1-col-1-content">
            <CustomAreaChart data={ graphData } height={ containerHeight * 0.8 } width={ containerWidth * 1.5 } />
          </div>
        </div>
        <div className="campaigns-row-1-col-2">
          <div className='campaigns-metrics-selector-wrapper'>
            <MetricsSelector metrics={dummyCampaignMetrics} />
          </div>
        </div>
        <div className="campaigns-row-2-col-1">
          <PlatformsTabs onPlatformSelect={this.onPlatformSelect} />
        </div>
        <div className="campaigns-row-3-col-1">
          <DetailedCampaignsList onSelectedCampaignsUpdate={this.onSelectedCampaignsUpdate} campaigns={campaignsData} columns={dummyCampaignsColumns} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  authUser: state.sessionState.authUser,
});

const authCondition = (authUser) => !!authUser;

const Standalone = Campaigns;
export {
  Standalone,
};

export default compose(
  withAuthorization(authCondition),
  connect(mapStateToProps),
  withRouter,
  Dimensions(),
)(Campaigns);