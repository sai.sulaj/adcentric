// --------------- DEPENDENCIES --------------- //

import React, {
  Component,
} from 'react';
import {
  Paper,
  withStyles,
  TableCell,
  TableSortLabel,
  Checkbox,
} from '@material-ui/core';
import { compose } from 'recompose';
import {
  AutoSizer,
  Column,
  SortDirection,
  Table,
} from 'react-virtualized';
import PropTypes from 'prop-types';
import classNames from 'classnames';

// --------------- LOCAL DEPENDENCIES --------------- //

import './DetailedCampaignsList.css';

// --------------- CONSTANTS --------------- //

const muiVTableStyles = {
  campaignsFlexContainer: {
    display   : 'flex',
    alignItems: 'center',
    boxSizing : 'border-box',
  },
  campaignsTableRow: {
    cursor: 'pointer',
  },
  campaignsTableRowHover: {
    '&:hover': {
      backgroundColor: '#F3F3F3',
    },
  },
  campaignsTableCell: {
    flex          : 1,
    fontSize      : 14,
    justifyContent: 'center',
    alignItems    : 'center',
  },
  campaignsTableNoClick: {
    cursor: 'initial',
  },
  campaignsTableHeader: {
    fontSize       : 16,
    backgroundColor: '#373a47',
    color          : '#FFFFFF',
    fontWeight     : 300,
  },
};

// --------------- VTABLE - MAIN --------------- //

class MuiVirtualizedTable extends React.PureComponent {
  getRowClassName = ({ index }) => {
    const { classes, rowClassName, onRowClick } = this.props;

    return classNames(classes.campaignsTableRow, classes.campaignsFlexContainer, rowClassName, {
      [classes.campaignsTableRowHover]: index !== -1 && onRowClick != null,
    });
  };

  cellRenderer = ({ cellData, rowIndex, columnIndex = null }) => {
    const {
      columns,
      classes,
      rowHeight,
      onRowClick,
    } = this.props;
    let content = null;
    if (columnIndex === 0) {
      content = columns[0].cell({ id: rowIndex, });
    } else {
      content = (cellData);
    }
    return (
      <TableCell
        component = "div"
        className = {classNames(classes.campaignsTableCell, classes.campaignsFlexContainer, {
          [classes.noClick]: onRowClick == null,
        })}
        variant = "body"
        style   = {{ height: rowHeight }}
        align   = {(columnIndex != null && columns[columnIndex].numeric) || false ? 'right' : 'left'}
        onClick = {(evt) => () => onRowClick(evt) /* No idea why the double param thingy is required but this is the only way it will work so fuck it. */}>
        {content}
      </TableCell>
    );
  };

  headerRenderer = ({ label, columnIndex, dataKey, sortBy, sortDirection }) => {
    const { headerHeight, columns, classes, sort } = this.props;
    const direction                                = {
      [SortDirection.ASC] : 'asc',
      [SortDirection.DESC]: 'desc',
    };

    const inner = 
      !columns[columnIndex].disableSort && sort != null ? (
        <TableSortLabel active={dataKey === sortBy} direction={direction[sortDirection]}>
          {typeof label === 'string' ? label.toUpperCase() : label}
        </TableSortLabel>
      ) : (
        typeof label === 'string' ? label.toUpperCase(): label
      );

    return (
      <TableCell
        component = "div"
        className = {classNames(classes.campaignsTableHeader, classes.campaignsTableCell, classes.campaignsFlexContainer, classes.campaignsTableNoClick)}
        variant   = "head"
        style     = {{ height: headerHeight }}
        align     = {columns[columnIndex].numeric || false ? 'right' : 'left'}>
        {inner}
      </TableCell>
    );
  };

  render() {
    const { classes, columns, ...tableProps } = this.props;
    return (
      <AutoSizer>
        {({ height, width }) => (
          <Table
            className = {classes.campaignsTable}
            height    = {height}
            width     = {width}
            {...tableProps}
            rowClassName = {this.getRowClassName}>
            {columns.map(({ cellContentRenderer = null, className, dataKey, ...other }, index) => {
              let renderer;
              if (cellContentRenderer != null) {
                renderer = cellRendererProps =>
                  this.cellRenderer({
                    cellData   : cellContentRenderer(cellRendererProps),
                    columnIndex: index,
                  });
              } else {
                renderer = this.cellRenderer;
              }

              return (
                <Column
                  key            = {dataKey}
                  headerRenderer = {headerProps =>
                    this.headerRenderer({
                      ...headerProps,
                      columnIndex: index,
                    })
                  }
                  className    = {classNames(classes.campaignsFlexContainer, className)}
                  cellRenderer = {renderer}
                  dataKey      = {dataKey}
                  {...other}
                />
              );
            })}
          </Table>
        )}
      </AutoSizer>
    );
  }
}

MuiVirtualizedTable.propTypes = {
  classes: PropTypes.object.isRequired,
  columns: PropTypes.arrayOf(
    PropTypes.shape({
      cellContentRenderer: PropTypes.func,
      dataKey            : PropTypes.string.isRequired,
      width              : PropTypes.number.isRequired,
    }),
  ).isRequired,
  headerHeight: PropTypes.number,
  onRowClick  : PropTypes.func,
  rowClassName: PropTypes.string,
  rowHeight   : PropTypes.oneOfType([PropTypes.number, PropTypes.func]),
  sort        : PropTypes.func,
};

MuiVirtualizedTable.defaultProps = {
  headerHeight: 40,
  rowHeight   : 40,
};

const WrappedMuiVirtualizationTable = withStyles(muiVTableStyles)(MuiVirtualizedTable);

// --------------- CAMPAIGNS TABLE - CONSTANTS --------------- //

const campaignsTableStyles = {
  campaignsTableRoot: {
    width : '100%',
    height: '100%',
  },
};

const INIT_STATE = {
  selectedCampaignIDs: [],
};

let customColumns = [
  {
    width   : 200,
    flexGrow: 1.0,
    label   : 'Name',
    dataKey : 'name',
  },
  {
    width   : 200,
    flexGrow: 1.0,
    label   : 'Status',
    dataKey : 'status',
  },
  {
    width   : 200,
    flexGrow: 1.0,
    label   : 'Start',
    dataKey : 'start',
  },
  {
    width   : 200,
    flexGrow: 1.0,
    label   : 'End',
    dataKey : 'end',
  },
  {
    width   : 200,
    flexGrow: 1.0,
    label   : 'Budget',
    dataKey : 'budget',
  },
  {
    width   : 200,
    flexGrow: 1.0,
    label   : 'Ads',
    dataKey : 'ads',
  },
  {
    width   : 200,
    flexGrow: 1.0,
    label   : 'IMPR.',
    dataKey : 'impressions',
  },
  {
    width   : 200,
    flexGrow: 1.0,
    label   : 'Click',
    dataKey : 'clicks',
  },
  {
    width   : 200,
    flexGrow: 1.0,
    label   : 'Spent',
    dataKey : 'spent',
  },
  {
    width   : 200,
    flexGrow: 1.0,
    label   : 'Conv.',
    dataKey : 'conversions',
  },
];

// --------------- CAMPAIGNS TABLE - MAIN --------------- //

class DetailedCampaignsList extends Component {
  constructor(props) {
    super(props);

    this.state = INIT_STATE;
  }

  doRowClick = ({rowData}) => {
    const { onSelectedCampaignsUpdate } = this.props;
    let   updatedSelectedCampaignIDs    = null;

    this.setState(prevState => {
      if (prevState.selectedCampaignIDs.some(id => rowData.id === id)) {
        // remove
        updatedSelectedCampaignIDs = prevState.selectedCampaignIDs.filter(
          id => id !== rowData.id
        );
        return {
          selectedCampaignIDs: updatedSelectedCampaignIDs,
        };
      } else {
        // add
        updatedSelectedCampaignIDs = [...prevState.selectedCampaignIDs, rowData.id];
        return {
          selectedCampaignIDs: updatedSelectedCampaignIDs,
        };
      }
    }, () => {
      if (onSelectedCampaignsUpdate !== null && onSelectedCampaignsUpdate !== undefined) {
        onSelectedCampaignsUpdate(updatedSelectedCampaignIDs);
      }
    });
  }

  buildColumns = () => {
    const { selectedCampaignIDs } = this.state;
    const { campaigns }           = this.props;

    return [{
      dataKey: 'checkbox',
      width  : 150,
      label  : (
        <Checkbox
          checked  = {selectedCampaignIDs.length > 0}
          onChange = {e => this.setState(prevState => {
            if (prevState.selectedCampaignIDs.length > 0) {
              return { selectedCampaignIDs: [], };
            } else {
              return { selectedCampaignIDs: campaigns.map(c => c.id) };
            }
          })}
          {...selectedCampaignIDs.length > 0
            && selectedCampaignIDs.length !== campaigns.length && {
              indeterminate: true,
              color        : 'default',
          }} />
      ),
      cell: rowData => (
        <Checkbox
          checked = {selectedCampaignIDs.some(id => rowData.id === id)}
        />
      )
    },
    ...customColumns];
  }

  render() {
    const {
      classes,
      campaigns,
    } = this.props;
    const data = campaigns != null ? campaigns : [];

    return (
      <Paper className={classes.campaignsTableRoot} elevation={2}>
        <WrappedMuiVirtualizationTable
          rowCount   = {data.length}
          rowGetter  = {({ index }) => data[index]}
          onRowClick = {this.doRowClick}
          columns    = {this.buildColumns()} />
      </Paper>
    );
  }
}

export default compose(
  withStyles(campaignsTableStyles),
)(DetailedCampaignsList);
