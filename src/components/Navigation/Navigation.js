// ---------- DEPENDENCIES ---------- //

import React, { Component } from 'react';
import { Navbar, Nav, NavItem, Glyphicon, NavDropdown } from 'react-bootstrap';
import { IndexLinkContainer } from 'react-router-bootstrap';
import { Animate } from 'react-simple-animate';
import { connect } from 'react-redux';

// ---------- LOCAL DEPENDENCIES ---------- //

// Constants.

import * as Routes from '../../constants/Routes';
import * as Actions from '../../constants/ActionTypes';

// Components.

import SignOutButton from '../SignOutButton';

// CSS.

import './Navigation.css';

// ---------- MAIN ---------- //

class Navigation extends Component {
  render() {
    const { authUser, onSetSidebarState } = this.props;

    return (
      <Navbar collapseOnSelect>
        {authUser ? (
          <NavigationAuth onSetSidebarState={onSetSidebarState} />
        ) : (
          <NavigationNonAuth />
        )}
      </Navbar>
    );
  }
}
// Navigation component visible when user is authenticated.
class NavigationAuthDumb extends Component {
  render() {
    const { onSetSidebarState, isSidebarOpen } = this.props;

    return (
      <Navbar.Collapse>
        <Navbar.Text className="center-children">
          <Animate play={isSidebarOpen} {...sidebarAnimationProps} durationSeconds={0.4}>
            <Glyphicon
              onClick={() => {
                onSetSidebarState(!isSidebarOpen);
              }}
              className = "sidebar-toggle-icon"
              glyph     = "list"
            />
          </Animate>
        </Navbar.Text>
        <Nav />

        <Nav pullRight>
          <NavDropdown id='more-dropdown' title="More">
            <IndexLinkContainer to={Routes.SETTINGS} exact>
              <NavItem>Settings</NavItem>
            </IndexLinkContainer>
            <SignOutButton />
          </NavDropdown>
        </Nav>
      </Navbar.Collapse>
    );
  }
}

const sidebarAnimationProps = {
  startStyle: {
    transform: 'rotate(0deg)',
  },
  endStyle: {
    transform: 'rotate(-90deg)',
  },
};

const mapNavDispatchToProps = dispatch => ({
  onSetSidebarState: isOpen =>
    dispatch({
      type   : Actions.SIDEBAR_STATE_SET,
      payload: { isOpen },
    }),
});

const mapNavStateToProps = state => ({
  isSidebarOpen: state.burgerMenu.isOpen,
});

const NavigationAuth = connect(
  mapNavStateToProps,
  mapNavDispatchToProps,
)(NavigationAuthDumb);

// Navigation component visible when user is not authenticated.
const NavigationNonAuth = () => (
  <Navbar.Collapse>
    <Nav>
      <IndexLinkContainer to={Routes.SIGN_IN} exact>
        <NavItem>Sign In</NavItem>
      </IndexLinkContainer>
    </Nav>
  </Navbar.Collapse>
);

// ---------- REDUX CONFIG ---------- //

const mapAuthStateToProps = state => ({
  authUser: state.sessionState.authUser,
});

const Standalone = Navigation;
export { Standalone };

export default connect(
  mapAuthStateToProps,
  null,
  null,
  { pure: false },
)(Navigation);
