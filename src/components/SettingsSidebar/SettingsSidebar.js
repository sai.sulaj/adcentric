// --------------- DEPENDENCIES --------------- //

import React, {
  PureComponent,
} from 'react';
import PropTypes from 'prop-types';

// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './SettingsSidebar.css';

// Constants.

import * as Tabs from '../../constants/SettingsTabs';

// --------------- MAIN --------------- //

class SettingsSidebar extends PureComponent {
  handleClick = (pageID) => {
    const { onChange } = this.props;
    onChange(pageID);
  }

  render() {
    const { selectedTab } = this.props;

    return (
      <div className='settings-sidebar-content-wrapper'>
        <div className={`settings-sidebar-item ${selectedTab === Tabs.PLATFORMS_TAB ? 'selected' : null}`}
          onClick={() => this.handleClick(Tabs.PLATFORMS_TAB)}>
          <p className='settings-sidebar-text'>
            Platforms
          </p>
        </div>
        <div className={`settings-sidebar-item ${selectedTab === Tabs.PROFILE_TAB ? 'selected' : null}`}
          onClick={() => this.handleClick(Tabs.PROFILE_TAB)}>
          <p className='settings-sidebar-text'>
            Profile
          </p>
        </div>
      </div>
    );
  }
}

SettingsSidebar.propTypes = {
  onChange: PropTypes.func.isRequired,
};

export default SettingsSidebar;
