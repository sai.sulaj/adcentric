// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';
import {
  List,
  ListItem,
  ListItemText,
  withStyles,
} from '@material-ui/core';

// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './AdAccountsList.css';

// --------------- CONSTANTS --------------- //

const materialStyles = {
  root: {
    width    : '100%',
  },
  listItem: {
    '& > span': {
      fontSize: 14,
    },
  },
};

// --------------- MAIN --------------- //

class AdAccountsList extends Component {
  /**
   * Handles ad account select from list. Calls passed 'onSelect' function
   * and passes ad account ID as parameter.
   *
   * @param {String} accountID -> Facebook ad account ID.
   */
  handleClick = (accountID) => {
    const { onSelect } = this.props;

    onSelect(accountID.replace('Account ID ', ''));
  }

  render() {
    const {
      classes,
      adAccounts,
    } = this.props

    return (
      <div className={classes.root}>
        <List component='nav'>
          {adAccounts.map((adAccount, i) => (
            <ListItem button
              key={i} onClick={() => this.handleClick(adAccount)}>
              <ListItemText className={classes.listItem} primary={adAccount} />
            </ListItem>
          ))}
        </List>
      </div>
    );
  }
}

export default withStyles(materialStyles)(AdAccountsList);
