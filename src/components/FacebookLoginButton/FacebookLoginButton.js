// ------------- DEPENDENCIES ------------- //

import React from 'react';

// ------------- LOCAL DEPENDENCIES ------------- //

import './FacebookLoginButton.css';

// ------------- MAIN ------------- //

const SignOutButton = ({ onClick }) => (
  <button
    onClick   = {onClick !== undefined ? onClick : () => {}}
    className = "btn btn-block btn-social btn-facebook">
    <span className="fa fa-facebook" /> Continue with Facebook
  </button>
);

export default SignOutButton;
