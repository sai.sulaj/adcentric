// ------------- DEPENDENCIES -------------- //

import React from 'react';
import { connect } from 'react-redux';

// ------------- LOCAL DEPENDENCIES -------------- //

// Constants.

import * as Actions from '../../constants/ActionTypes';
import * as PlatformIDs from '../../constants/PlatformIDs';
import * as DatabaseFields from '../../constants/DatabaseFields';

// Components.

import {
  Firebase,
  Db,
} from '../../firebase';

// ------------- MAIN -------------- //

/**
 * Component wrapper that providers authUser object when available to
 * component.
 *
 * @param {React.Component} Component -> Arbitrary component that requires
 *                                       authentication to access.
 *
 * @return {React.Component} Component -> Passed component wrapped with
 *                                        helper.
 */
const withAuthentication = Component => {
  class WithAuthentication extends React.Component {
    componentDidMount = () => {
      const {
        onSetAuthUser,
        onSetPlatformsAuthState,
      } = this.props;

      Firebase.FirebaseAuth.onAuthStateChanged(authUser => {
        authUser ? onSetAuthUser(authUser): onSetAuthUser(null);

        if (authUser) {
          Db.doSetLastLogin(authUser.uid);

          Db.doFetchUser(authUser.uid).then((doc) => {
            if (!doc.exists) {
              console.log(`ERROR: User document does not exist.`);
            } else {
              const userData = doc.data();
              if (userData[PlatformIDs.FACEBOOK] !== undefined && userData[PlatformIDs.FACEBOOK][DatabaseFields.ID] !== undefined) {
                onSetPlatformsAuthState(PlatformIDs.FACEBOOK, true);
              }
              // TODO: Implement.
              /*
              if (userData.instagram !== undefined && userData.instagramID !== undefined) {
                onSetPlatformsAuthState(PlatformIDs.INSTAGRAM, true);
              }
              if (userData.linkedIn !== undefined && userData.linkedInID !== undefined) {
                onSetPlatformsAuthState(PlatformIDs.LINKED_IN, true);
              }
              if (userData.google !== undefined && userData.googleID !== undefined) {
                onSetPlatformsAuthState(PlatformIDs.GOOGLE, true);
              }*/
            }
          });
        }
      });
    };

    render() {
      return <Component />;
    }
  }

  const mapDispatchToProps = dispatch => ({
    onSetAuthUser: authUser =>
      dispatch({
        type    : Actions.AUTH_USER_SET,
        authUser: authUser,
      }),
    onSetPlatformsAuthState: (platformID, platformAuthState) =>
      dispatch({
        type: Actions.PLATFORMS_AUTH_STATE_SET,
        platformID,
        platformAuthState,
      }),
  });

  return connect(
    null,
    mapDispatchToProps,
  )(WithAuthentication);
};

export default withAuthentication;
