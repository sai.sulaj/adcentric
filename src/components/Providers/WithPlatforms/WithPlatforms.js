// ------------- DEPENDENCIES -------------- //

import { compose } from 'recompose';

// ------------- LOCAL DEPENDENCIES ------------- //

import WithFacebook from './WithFacebook';

export default compose(
  WithFacebook,
);