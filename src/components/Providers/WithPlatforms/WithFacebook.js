// ------------- DEPENDENCIES ------------ //

import React from 'react';
import { connect } from 'react-redux';

// ------------- LOCAL DEPENDENCIES ------------ //

import { FacebookAPI } from '../../../facebook';
import { Db, } from '../../../firebase';

// Constants.

import * as DatabaseFields from '../../../constants/DatabaseFields';

// ------------- CONSTANTS ------------ //

import * as Actions from '../../../constants/ActionTypes';
import * as PlatformIDs from '../../../constants/PlatformIDs';

// ------------- MAIN ------------ //

const withFacebook = Component => {
  class WithFacebook extends React.Component {
    componentDidUpdate = (prevProps, prevState) => {
      const { authUser } = this.props;

      if (prevProps.authUser !== authUser) {
        this.handleFacebookAPIUpdate();
      }
    }

    handleFacebookAPIUpdate = (retry) => {
      const {
        authUser,
        onSetFacebookAPI,
        onUnsetFacebookAPI,
        onSetFacebookUser,
        onSetFacebookAPISelectedAdAccountID,
        api,
      } = this.props;

      if (retry > 2) return;
      if (api === null && authUser !== null && authUser !== undefined) {
        Db.doFetchUser(authUser.uid).then((doc) => {
          if (!doc.exists) {
            console.log('ERROR: Missing Firebase user document.');
            setTimeout(() => {
              this.handleFacebookAPIUpdate(retry === undefined ? 0 : ++retry);
            }, 1000);
          } else {
            const userData = doc.data();
            console.log(userData);
            if (userData[PlatformIDs.FACEBOOK] !== undefined) {
              if (userData[PlatformIDs.FACEBOOK][DatabaseFields.ACCESS_TOKEN] !== undefined) {
                const api = FacebookAPI.initAPI(userData.facebook[DatabaseFields.ACCESS_TOKEN]);
                onSetFacebookAPI({
                  api,
                  available: true,
                });
                onSetFacebookUser({
                  ...userData[PlatformIDs.FACEBOOK]
                });
              }
              if (userData[PlatformIDs.FACEBOOK].selectedAdAccountID !== undefined) {
                onSetFacebookAPISelectedAdAccountID(userData[PlatformIDs.FACEBOOK].selectedAdAccountID);
              }
            }
          }
        });
      } else if (authUser === null || authUser === undefined) {
        onUnsetFacebookAPI();
      }
    }

    render() {
      return <Component />
    }
  }

  const mapStateToProps = state => ({
    authUser: state.sessionState.authUser,
    api     : state.facebookAPIState.api,
  });

  const mapDispatchToProps = dispatch => ({
    onSetFacebookAPI: facebookAPIState =>
      dispatch({
        type: Actions.FACEBOOK_API_SET,
        ...facebookAPIState,
      }),
    onUnsetFacebookAPI: () =>
      dispatch({
        type: Actions.FACEBOOK_API_UNSET,
      }),
    onSetFacebookUser: facebookUser =>
      dispatch({
        type        : Actions.FACEBOOK_USER_SET,
        facebookUser: facebookUser,
      }),
    onSetFacebookAPISelectedAdAccountID: adAccountID =>
      dispatch({
        type               : Actions.FACEBOOK_API_SELECTED_AD_ACCOUNT_ID,
        selectedAdAccountID: adAccountID,
      }),
  });

  return connect(
    mapStateToProps,
    mapDispatchToProps,
  )(WithFacebook);
}

export default withFacebook;