// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';
import { compose } from 'recompose';
import {
  withStyles,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  Button,
  Checkbox,
} from '@material-ui/core';
import {
  MenuRounded,
} from '@material-ui/icons';

// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './DemographicControlCampaigns.css';

// Constants.

import { COMPONENTS_ENUM } from '../DemographicControl';

// --------------- CONSTANTS --------------- //

const materialStyles = {
  root: {
    height   : '100%',
    width    : '100%',
    textAlign: 'center',
  },
  header: {
  },
  headerIcon: {
    position: 'absolute',
    left    : 25,
  },
  headerWrapper: {
    padding     : '15px 0 15px 0px',
    margin      : '0 !important',
    borderRadius: '4px 4px 0 0',
    background  : '#FFFFFF',
    color       : '#373a47',
    '&:hover'   : {
      backgroundColor: '#F3F3F3',
    },
  },
  listRoot: {
    maxHeight: '70vh',
    overflow : 'auto',
  },
  selectAllButton: {
    marginTop: 5,
    width    : '90%',
    '&:hover': {
      textDecoration        : 'none',
      backgroundColor       : '#F45B691F',
      '@media (hover: none)': {
        backgroundColor: 'transparent',
      },
      '&:disabled': {
        backgroundColor: 'transparent',
      },
    }
  }
};

// Initial state of component.
const INIT_STATE = {
  checked: [], // {Array<number>} -> Campaign IDs that are checked.
};

// --------------- MAIN --------------- //

class DemographicControlCampaigns extends Component {
  constructor(props) {
    super(props);

    this.state = INIT_STATE;
  }

  /**
   * Check all items in list.
   */
  checkAll = () => {
    const { campaigns } = this.props;
    const { checked }   = this.state;

    if (checked.length > 0) {
      this.setState({
        checked: [],
      });
    } else {
      this.setState({
        checked: campaigns.map((campaign) => {
          return campaign.number;
        })
      });
    }
  }

  /**
   * Toggles checked state of list item.
   *
   * @param {boolean} -> Checked state of list item.
   */
  handleToggle = value => () => {
    const { checked } = this.state;
    const currIndex   = checked.indexOf(value);
    const newChecked  = [...checked];

    if (currIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currIndex, 1);
    }

    this.setState({
      checked: newChecked,
    });
  }

  /**
   * Sets this campaigns component as active & visible.
   */
  doToggleActive = () => {
    const { toggleActive } = this.props;

    toggleActive(COMPONENTS_ENUM.CAMPAIGNS);
  }

  render() {
    const { checked }             = this.state;
    const { classes, campaigns, } = this.props;

    return (
      <div className={classes.root}>
        <div onClick={() => this.doToggleActive()} className={classes.headerWrapper}>
          <MenuRounded className={classes.headerIcon} />
          <div className={classes.header}>Campaigns</div>
        </div>
        <List className={`${classes.listRoot} collapsible`}>
          {(campaigns !== undefined && campaigns !== null ? campaigns : []).map((campaign, i) => {
            return  (
              <ListItem key={i}>
                <ListItemText primary={`Campaign ${campaign.number}`} />
                <ListItemSecondaryAction>
                  <Checkbox
                    onChange = {this.handleToggle(campaign.number)}
                    checked  = {checked.indexOf(campaign.number) !== -1} />
                </ListItemSecondaryAction>
              </ListItem>
            );
          })}
        </List>
        <Button
          onClick={this.checkAll} className={`${classes.selectAllButton} hidable`}>
          {checked.length > 0 ? 'Clear' : 'Select all'}
        </Button>
      </div>
    );
  }
}

export default compose(
  withStyles(materialStyles),
)(DemographicControlCampaigns);
