// ------------- DEPENDENCIES ------------- //

import React, { Component } from 'react';
import {
  FormGroup,
  ControlLabel,
  FormControl,
  InputGroup,
  Button,
  Glyphicon,
} from 'react-bootstrap';
import { Link, withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import { connect } from 'react-redux';

// ------------- LOCAL DEPENDENCIES ------------- //

// CSS.
import './SignInForm.css';

// Constants.

import * as Routes from '../../constants/Routes';

// Components.

import { Auth } from '../../firebase';

// ------------- MAIN ------------- //

// Constants.

const INIT_STATE = {
  // Initial state of component.
  email                  : '',     // User's email.
  password               : '',     // User's password.
  error                  : null,   // Sign in error.
  emailValidationState   : null,
  passwordValidationState: null,
};

class SignInForm extends Component {
  constructor(props) {
    super(props);

    this.state = INIT_STATE;
  }

  /**
   * Accesses email value in state, and computes validation
   * state for bootstrap FormGroup field. TODO: Figure out wtf
   * the regex expression means.
   *
   * @param {String} email -> Email value.
   *
   * @return -> 'success' if valid, 'error' otherwise.
   */
  emailValidationState = email => {
    // eslint-disable-next-line
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    return re.test(`${email}`.toLowerCase()) ? 'success': 'error';
  };

  /**
   * Accesses password value in state, and computes validation
   * state for bootstrap FormGroup field. Valid if field is
   * not an empty string.
   *
   * @param {String} password -> Password value.
   *
   * @return -> 'success' if valid, 'error' otherwise.
   */
  passwordValidationState = password => {
    return password !== '' ? 'success': 'error';
  };

  /**
   * Set's email value in component state,
   * and updates emailValidationState in component state.
   *
   * @param {String} value -> Value to set in component state.
   */
  handleEmailFieldChange = value => {
    this.setState({
      email               : value,
      emailValidationState: this.emailValidationState(value),
    });
  };

  /**
   * Set's password value in component state,
   * and updates passwordOneValidationState in component state.
   *
   * @param {String} value -> Value to set in component state.
   */
  handlePasswordFieldChange = value => {
    this.setState({
      password               : value,
      passwordValidationState: this.passwordValidationState(value),
    });
  };

  /**
   * Fetches email and password values im component state, and
   * signs user in to Firebase Auth. Does no input validation. On
   * success, navigates to OVERVIEW page. On error, sets 'error' value
   * in component state with returned error. Meant for 'onClick' button
   * event callback.
   *
   * @param {JSON} event -> 'onClick' button event data object.
   */
  onSubmit = event => {
    const { email, password } = this.state;
    const { history }         = this.props;

    Auth.doSignInWithEmailAndPassword(email, password)
      .then(() => {
        this.setState({
          ...INIT_STATE,
        });
        history.push(Routes.OVERVIEW);
      })
      .catch(error => {
        this.setState({
          error: error,
        });
      });

    event.preventDefault();
  };

  render() {
    const { email, password, error, emailValidationState, passwordValidationState } = this.state;

    // For submit button 'disabled' prop.
    const isValid = emailValidationState === 'success' && passwordValidationState === 'success';

    return (
      <div>
        <form className="form-horizontal">
          <FormGroup controlId="signin-email-field" validationState={emailValidationState}>
            <ControlLabel className="signin-form-label">Your Email</ControlLabel>
            <InputGroup>
              <InputGroup.Addon>
                <Glyphicon glyph="envelope" />
              </InputGroup.Addon>
              <FormControl
                autoComplete = "email"
                id           = "signin-email-field"
                type         = "email"
                value        = {email}
                placeholder  = "Email"
                onChange     = {evt => this.handleEmailFieldChange(evt.target.value)}
              />
              <FormControl.Feedback />
            </InputGroup>
          </FormGroup>

          <FormGroup controlId="signin-password-field" validationState={passwordValidationState}>
            <ControlLabel className="signin-form-label">Your Password</ControlLabel>
            <InputGroup>
              <InputGroup.Addon>
                <Glyphicon glyph="lock" />
              </InputGroup.Addon>
              <FormControl
                autoComplete = "password"
                id           = "signin-password-field"
                type         = "password"
                value        = {password}
                placeholder  = "Password"
                onChange     = {evt => this.handlePasswordFieldChange(evt.target.value)}
              />
              <FormControl.Feedback />
            </InputGroup>
          </FormGroup>

          <Button
            type      = "submit"
            bsStyle   = "primary"
            className = "signin-submit-button"
            disabled  = {!isValid}
            onClick   = {this.onSubmit}>
            Login
          </Button>

          <div className="signin-signup-link-wrapper">
            <Link to={Routes.SIGN_UP} className="signin-signup-link">
              Sign Up
            </Link>
          </div>

          {error && <p>{error.message}</p>}
        </form>
      </div>
    );
  }
}

const Standalone = SignInForm;
export { Standalone };

export default compose(
  connect(
    null,
    null,
  ),
  withRouter,
)(SignInForm);
