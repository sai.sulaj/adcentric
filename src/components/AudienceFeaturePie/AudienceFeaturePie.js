// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';
import { compose } from 'recompose';
import {
  VictoryPie,
  VictoryAnimation,
  VictoryLabel,
} from 'victory';
import Dimensions from 'react-dimensions';

// --------------- LOCAL DEPENDENCIES --------------- //

import './AudienceFeaturePie.css';

// --------------- CONSTANTS --------------- //


// --------------- MAIN --------------- //

class AudienceFeaturePie extends Component {
  render() {
    const {
      containerHeight,
      containerWidth,
      useWidth,
      data,
      label,
      mult,
    } = this.props;
    let dim  = useWidth ? containerWidth : containerHeight;
        dim *= mult !== undefined && mult !== null ? mult : 1;

    return (
      <svg viewBox={`0 0 ${dim} ${dim}`} width={dim} height={dim}>
        <VictoryPie
          standalone   = {false}
          animate      = {{ duration: 1000 }}
          width        = {dim} height = {dim}
          data         = {data}
          innerRadius  = {dim * 0.25}
          cornerRadius = {dim * 0.0625}
          labels       = {() => null}
          style        = {{
            data: { fill: (d) => {
              const color = d.y > 50 ? "pink" : "blue";
              return color;
            }
            }
          }}
        />
        <VictoryAnimation duration={1000} data={this.props}>
          {(newProps) => {
            return (
              <VictoryLabel
                textAnchor = "middle" verticalAnchor = "middle"
                x          = {dim / 2} y             = {dim / 2}
                text       = {`${Math.round(data[0].y)}%`}
                style      = {{ fontSize: 17 }}
              />
            );
          }}
        </VictoryAnimation>
      </svg>
    );
  }
}

export default compose(
  Dimensions(),
)(AudienceFeaturePie);
