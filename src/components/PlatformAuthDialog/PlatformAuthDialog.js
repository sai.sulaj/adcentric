// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  withStyles,
} from '@material-ui/core';
import { compose } from 'recompose';
import { connect } from 'react-redux';

// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './PlatformAuthDialog.css';

// Components.

import FacebookAuth from '../FacebookAuth';
import FacebookLoginButton from '../FacebookLoginButton';

// APIs.

import { Db } from '../../firebase';

// Constants.

import * as Actions from '../../constants/ActionTypes';

// --------------- CONSTANTS --------------- //

const materialStyles = {
  root: {},
  title: {
    '& > h2': {
      fontSize: 20,
    },
  },
  text: {
    marginBottom: 10,
  },
  buttonWrapper: {
    padding: '0 30px 40px 30px',
  },
};

// --------------- MAIN --------------- //

class PlatformAuthDialog extends Component {
  /**
   * Facebook authorization success callback. Updates redux
   * State with user's Facebook data.
   *
   * @param {JSON} user -> Facebook user data object.
   * @param {JSON} resp -> Facebook API response object.
   */
  onFBLoginSuccess = (user, resp) => {
    const {
      onSetFacebookUser,
      authUser,
      onUpdateFacebookAPIAccessToken,
      onClearPlatformAuthError,
    } = this.props;

    onSetFacebookUser(user);
    onUpdateFacebookAPIAccessToken(user.accessToken);
    Db.doUpdateFacebookUser(authUser.uid, user);
    onClearPlatformAuthError();
  };

  /**
   * Facebook authorization error callback.
   *
   * @param {JSON} err -> Error object.
   */
  onFBLoginError = (err) => {
    // TODO.
    console.error(err);
  };

  render() {
    const {
      classes,
      //platformID, // TODO: Use to select platform login.
      message,
      open,
      onClose,
    } = this.props

    return (
      <Dialog
        className={classes.root} open={open} onClose={onClose}>
        <DialogTitle className={classes.title}>Refresh the Session</DialogTitle>
        <DialogContent className={classes.buttonWrapper}>
          <DialogContentText className={classes.text}>
            {message}
          </DialogContentText>
          <FacebookAuth
            callback  = {this.onFBLoginSuccess}
            trigger   = {FacebookLoginButton}
            onFailure = {this.onFBLoginError} />
        </DialogContent>
      </Dialog>
    );
  }
}

const mapStateToProps = state => ({
  platformAuthErrorState: state.platformAuthErrorState,     // {String} -> Platform authentication error state.
  authUser              : state.sessionState.authUser,      // {Object} -> Firebase user data.
});

const mapDispatchToProps = dispatch => ({
  onSetFacebookUser: facebookUser =>    // Sets Facebook user redux state.
    dispatch({
      type        : Actions.FACEBOOK_USER_SET,
      facebookUser: facebookUser,
    }),
  onUpdateFacebookAPIAccessToken: accessToken =>   // Sets redux Facebook user access token state.
    dispatch({
      type          : Actions.FACEBOOK_API_ACCESS_TOKEN_UPDATE,
      newAccessToken: accessToken,
    }),
  onClearPlatformAuthError: () =>    // Clears redux platform authentication error.
    dispatch({
      type: Actions.PLATFORM_AUTH_ERROR_CLEAR,
    }),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withStyles(materialStyles),
)(PlatformAuthDialog);
