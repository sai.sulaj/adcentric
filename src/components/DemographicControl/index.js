import DemographicControl, {
  COMPONENTS_ENUM,
} from './DemographicControl';

export default DemographicControl;

export {
  COMPONENTS_ENUM,
};