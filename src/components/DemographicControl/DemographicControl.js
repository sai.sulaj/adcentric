// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';
import { compose } from 'recompose';
import {
  withStyles,
  Paper,
  Button,
} from '@material-ui/core';
import _ from 'lodash';

// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './DemographicControl.css';

// Components.

import DemographicControlCampaign from '../DemographicControlCampaigns';
import DemographicControlSavedCampaignGroups from '../DemographicControlSavedCampaignGroups';

// --------------- CONSTANTS --------------- //

const demographicControlStyles = theme => ({
  demographicControlRoot: {
    height        : '100%',
    width         : '100%',
    display       : 'flex',
    flexFlow      : 'column nowrap',
    justifyContent: 'flex-end',
    alignItems    : 'center',
    padding       : 10,
    '& *'         : {
      '-webkit-transition': 'max-height 300ms ease-in-out, flex-grow 300ms ease-in-out',
      '-moz-transition'   : 'max-height 300ms ease-in-out, flex-grow 300ms ease-in-out',
      '-ms-transition'    : 'max-height 300ms ease-in-out, flex-grow 300ms ease-in-out',
      '-o-transition'     : 'max-height 300ms ease-in-out, flex-grow 300ms ease-in-out',
      'transition'        : 'max-height 300ms ease-in-out, flex-grow 300ms ease-in-out',
    },
  },
  demographicControlCampaign: {
    width     : '100%',
    margin    : '5px',
    minHeight : 30,
    '&.active': {
      flexGrow: 1,
    },
    '&.inactive .collapsible': {
      maxHeight: '0 !important',
      padding  : 0,
    },
    '&.inactive .hidable': {
      display: 'none',
    },
  },
  demographicControlSaved: {
    width     : '100%',
    margin    : '5px',
    minHeight : 30,
    '&.active': {
      flexGrow: 1,
    },
    '&.inactive .collapsible': {
      maxHeight: '0 !important',
      padding  : 0,
    },
    '&.inactive .hidable': {
      display: 'none',
    },
  },
  demographicControlLaunch: {
    margin         : '5px',
    backgroundColor: '#F45B69',
    color          : '#373a47',
    fontSize       : 18,
  },
});

// Initial state of component.
const INIT_STATE = {
  activeComponent: null, // {String} -> active component.
  campaignsData  : null, // {Array<Object>} -> List of campaigns data.
};

/**
 * Enum of component ids that could be active & visible.
 */
export const COMPONENTS_ENUM = {
  CAMPAIGNS      : 'campaigns',
  SAVED_CAMPAIGNS: 'saved_campaigns',
};

// --------------- MAIN --------------- //

class DemographicControl extends Component {
  constructor(props) {
    super(props);

    this.state = INIT_STATE;
  }

  componentDidMount = () => {
    // Load campaign data from Facebook API.
    this.setState({
      campaignsData: this.getCampaignsData(),
    });
  }
  
  /**
   * Function to set the active and visible component.
   * To be passed to children so UI to change active
   * component can be built.
   *
   * @param {string} componentID -> ID of component to be set active.
   */
  setActiveComponent = (componentID) => {
    this.setState(prevState => {
      if (prevState.activeComponent === componentID) {
        return {
          activeComponent: null,
        };
      } else {
        return {
          activeComponent: componentID,
        };
      }
    });
  }

  /**
   * Fetch user campaigns from Facebook API.
   *
   * @return {Array<Object>} -> List of campaign objects.
   */
  getCampaignsData = () => {
    return _.range(20).map((ph, i) => {
      return { number: i + 1 };
    })
  }

  render() {
    const { classes } = this.props;
    const {
      activeComponent,
      campaignsData,
    } = this.state;

    // Determine active component.
    const isCampaignsActive      = activeComponent === COMPONENTS_ENUM.CAMPAIGNS;
    const isSavedCampaignsActive = activeComponent === COMPONENTS_ENUM.SAVED_CAMPAIGNS;

    return (
      <div className={classes.demographicControlRoot}>
        <Paper
          className={`${classes.demographicControlCampaign} ${isCampaignsActive ? 'active' : 'inactive'}`}>
          <DemographicControlCampaign campaigns={campaignsData} isActive={isCampaignsActive}
            toggleActive={this.setActiveComponent} />
        </Paper>
        <Paper
          className={`${classes.demographicControlSaved} ${isSavedCampaignsActive ? 'active' : 'inactive'}`}>
          <DemographicControlSavedCampaignGroups campaigns={campaignsData} isActive={isCampaignsActive}
            toggleActive={this.setActiveComponent} />
        </Paper>
        <Button variant="contained" className={classes.demographicControlLaunch} fullWidth>
          Launch
        </Button>
      </div>
    );
  }
}

export default compose(
  withStyles(demographicControlStyles),
)(DemographicControl);
