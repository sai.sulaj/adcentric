// --------------- DEPENDENCIES --------------- //

import React, {
  Component,
  Fragment,
} from 'react'
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Collapse,
  withStyles,
} from '@material-ui/core';
import {
  AddRounded,
} from '@material-ui/icons';

// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './Faq.css';

// --------------- CONSTANTS --------------- //

const materialStyles = {
  root    : {},
  faqTitle: {
    '& > span': {
      fontSize: 18,
    },
  },
  faqTitleWrapper  : {},
  faqContentWrapper: {},
  faqContent       : {
    margin: 10,
  },
};

const highlightStyles = {
  backgroundColor: '#F45B6955',
};

// --------------- MAIN --------------- //

class Faq extends Component {
  /**
   * Toggles open state of content container.
   *
   * @param {number} index -> Index of container to toggle.
   */
  toggleContentOpen = (index) => {
    const { onContentOpenStateUpdate } = this.props;
    onContentOpenStateUpdate(index);
  }

  /**
   * Returns span with searched text highlighted.
   *
   * @param {String} text -> Text to search.
   * @param {String} highlight -> Search query text.
   *
   * @return {HTML} -> Span with text highlighted.
   */
  getHighlightedText = (text, highlight) => {
    let parts = text.split(new RegExp(`(${highlight})`, 'gi'));
    return <span> { parts.map((part, i) =>
        <span key={i} style={part.toLowerCase() === highlight.toLowerCase() ? highlightStyles : {} }>
            { part }
        </span>)
    } </span>;
  }

  render() {
    const {
      classes,
      faqContent,
      faqContentOpenState,
      searchValue,
    } = this.props;

    return (
      <List className={classes.root}>
        {faqContent.map((faq, i) => (
          <Fragment key={i}>
            <ListItem className={classes.faqTitleWrapper} divider button onClick={() => this.toggleContentOpen(i)}>
              <ListItemText className={classes.faqTitle} primary={faq.title} />
              <ListItemIcon>
                <AddRounded />
              </ListItemIcon>
            </ListItem>
            <Collapse className={classes.faqContentWrapper} in={faqContentOpenState[i]} timeout='auto' unmountOnExit>
              <p className={classes.faqContent}>{searchValue.length > 3 ? this.getHighlightedText(faq.content, searchValue) : faq.content}</p>
            </Collapse>
          </Fragment>
        ))}
      </List>
    );
  }
}

export default withStyles(materialStyles)(Faq);
