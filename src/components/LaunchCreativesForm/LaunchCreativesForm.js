// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';
import {
  Paper,
  TextField,
  withStyles,
} from '@material-ui/core';
import {
  DropzoneArea,
} from 'material-ui-dropzone';

// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './LaunchCreativesForm.css';

// Constants.

import { FORM_TYPE_ENUM } from '../Pages/Launch';

// --------------- CONSTANTS --------------- //

const materialStyles = {
  root: {
    width       : '100%',
    padding     : 20,
    marginBottom: 20,
  },
  formHeader: {
    marginTop : '5px !important',
    fontWeight: 300,
  },
  formContainer: {
    display       : 'flex',
    flexFlow      : 'column nowrap',
    justifyContent: 'center',
    alignItems    : 'center',
    padding       : '0 40px',
  },
  textField: {
    '& input': {
      fontSize: 14,
    },
  },
};

const INPUT_VARIANT = 'standard';    // Material UI text input variant.

// --------------- MAIN --------------- //

class LaunchCreativesForm extends Component {
  render() {
    const {
      classes,
      campaignAdHeadlineInput,
      campaignAdTextInput,
      onFormChange,
    } = this.props

    return (
      <Paper className={classes.root} elevation={2}>
        <h3 className={classes.formHeader}>Creatives</h3>
        <form className={classes.formContainer}>
          <TextField fullWidth required
            id          = 'campaign-ad-headline-input'
            label       = 'Ad headline'
            placeholder = "Set your ad's headline..."
            className   = {classes.textField}
            value       = {campaignAdHeadlineInput}
            onChange    = {(evt) => { onFormChange(FORM_TYPE_ENUM.CREATIVES, 'campaignAdHeadlineInput', evt.target.value); }}
            margin      = "normal"
            variant     = {INPUT_VARIANT} />
          <TextField fullWidth required
            id          = 'campaign-ad-text-input'
            label       = 'Ad text'
            placeholder = "Set your ad's text..."
            className   = {classes.textField}
            value       = {campaignAdTextInput}
            onChange    = {(evt) => { onFormChange(FORM_TYPE_ENUM.CREATIVES, 'campaignAdTextInput', evt.target.value); }}
            margin      = "normal"
            variant     = {INPUT_VARIANT} />
          <DropzoneArea
            dropZoneClass = 'launch-creatives-form-images-dropdown'
            acceptedFiles = {['image/*']} filesLimit = {1} showFileNamesInPreview = {true}
            onChange      = {(files) => onFormChange(FORM_TYPE_ENUM.CREATIVES, 'campaignImageFilesInput', files)}  />
        </form>
      </Paper>
    );
  }
}

export default withStyles(materialStyles)(LaunchCreativesForm);
