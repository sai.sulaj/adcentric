// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';
import {
  compose,
} from 'recompose';
import {
  Paper,
  FormControl,
  InputLabel,
  Input,
  InputAdornment,
  Menu,
  MenuItem,
  List,
  ListItem,
  ListItemText,
  withStyles,
} from '@material-ui/core';
import MomentUtils from '@date-io/moment';
import {
  MuiPickersUtilsProvider,
  DatePicker,
} from 'material-ui-pickers';
import Moment from 'moment';

// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './LaunchSetBudgetForm.css';

// Constants.

import { FORM_TYPE_ENUM } from '../Pages/Launch';

// --------------- CONSTANTS --------------- //

const materialStyles = {
  root: {
    width       : '100%',
    padding     : 20,
    marginBottom: 20,
  },
  formHeader: {
    marginTop : '5px !important',
    fontWeight: 300,
  },
  formContainer: {
    display       : 'flex',
    flexFlow      : 'column nowrap',
    justifyContent: 'center',
    alignItems    : 'center',
    padding       : '0 40px',
  },
  textField: {
    '& input': {
      fontSize: 14,
    },
  },
  budgetTypeRoot: {
    width: '100%',
  },
  campaignDateRangeWrapper: {
    display       : 'flex',
    flexFlow      : 'row nowrap',
    width         : '100%',
    justifyContent: 'space-around',
    alignItems    : 'center',
    '& > *': {
      width : '85%',
      margin: '0 10px',
    },
  },
};

const INIT_STATE = {
  budgetValueValue       : 0,       // {number} -> User budget value input.
  selectedBudgetTypeIndex: 0,       // {number} -> budgetOptions index of selected budget type.
  anchorEl               : null,
};

const INPUT_VARIANT = 'standard';   // Material UI text input variant.

const currencyInputProps = {
  min: 0,
};

const budgetOptions = [
  { label: 'Daily', value: 'daily_budget'},
  { label: 'Lifetime', value: 'lifetime_budget'}
];

// --------------- MAIN --------------- //

class LaunchSetBudgetForm extends Component {
  constructor(props) {
    super(props);

    this.state = INIT_STATE;
  }

  componentDidMount = () => {
    this.setInitState();
  }
  
  /**
   * Set's parent initial state.
   */
  setInitState = () => {
    const { onFormChange, } = this.props;
    onFormChange(FORM_TYPE_ENUM.BUDGET, 'campaignBudgetType', budgetOptions[0].value);
    onFormChange(FORM_TYPE_ENUM.BUDGET, 'campaignStartDate', Moment());
    onFormChange(FORM_TYPE_ENUM.BUDGET, 'campaignEndDate', Moment().add(1, 'weeks'));
  }

  /**
   * Opens budget type select list.
   */
  handleBudgetTypeOpen = evt => {
    this.setState({ anchorEl: evt.currentTarget });
  }

  /**
   * Closes budget type select list.
   */
  handleBudgetTypeClose = () => {
    this.setState({ anchorEl: null });
  }

  /**
   * Handles budget type select.
   *
   * @param {JSON} evt -> Button click event.
   * @param {number} index -> Selected budget type index in budgetOptions.
   */
  handleBudgetTypeItemClick = (evt, index) => {
    const { onFormChange } = this.props;
    onFormChange(FORM_TYPE_ENUM.BUDGET, 'campaignBudgetType', budgetOptions[index].value);
    this.setState({ selectedBudgetTypeIndex: index, anchorEl: null });
  }

  render() {
    const {
      classes,
      onFormChange,
      campaignBudgetValue,
      campaignStartDate,
      campaignEndDate,
    } = this.props
    const {
      selectedBudgetTypeIndex,
      anchorEl,
    } = this.state;

    const minEndDate = Moment(campaignStartDate).add(1, 'days');

    return (
      <MuiPickersUtilsProvider utils={MomentUtils}>
        <Paper className={classes.root} elevation={2}>
          <h3 className={classes.formHeader}>Budget</h3>
          <form className={classes.formContainer}>
            <FormControl fullWidth
              className={classes.textField}>
              <InputLabel htmlFor='campaign-name-input'>Budget</InputLabel>
              <Input required
                id          = 'campaign-budget-input'
                label       = 'Budget'
                placeholder = 'Set your campaign budget...'
                type        = 'number'
                className   = {classes.textField}
                value       = {campaignBudgetValue}
                inputProps  = {currencyInputProps}
                onChange    = {(evt) => { onFormChange(FORM_TYPE_ENUM.BUDGET,
                                                      'campaignBudgetValue',
                                                      evt.target.value) }}
                variant     = {INPUT_VARIANT}
                startAdornment={<InputAdornment position="start">$</InputAdornment>}
              />
            </FormControl>
            <div className={classes.budgetTypeRoot}>
              <List component='nav'>
                <ListItem button
                  aria-haspopup='true'
                  aria-controls='budget-type'
                  aria-label='Select budget type'
                  onClick={this.handleBudgetTypeOpen}>
                  <ListItemText
                    primary='Select budget type'
                    secondary={budgetOptions[selectedBudgetTypeIndex].label} />
                </ListItem>
              </List>
              <Menu fullWidth
                id='budget-type'
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
                onClose={this.handleBudgetTypeClose}>
                {budgetOptions.map((option, i) => (
                  <MenuItem
                    key={option.label}
                    selected={i === selectedBudgetTypeIndex}
                    onClick={evt => this.handleBudgetTypeItemClick(evt, i)}>
                    {option.label}
                  </MenuItem>
                ))}
              </Menu>
            </div>
            <div className={classes.campaignDateRangeWrapper}>
              <DatePicker value={campaignStartDate} label='Start Date'
                minDate={Moment()} minDateMessage='Start date cannot be in the past.'
                onChange={(date) => onFormChange(FORM_TYPE_ENUM.BUDGET, 'campaignStartDate', date)} />
              <DatePicker value={campaignEndDate} label='End Date'
                minDate={minEndDate} minDateMessage='End date must be after the start date.'
                onChange={(date) => onFormChange(FORM_TYPE_ENUM.BUDGET, 'campaignEndDate', date)} />
            </div>
          </form>
        </Paper>
      </MuiPickersUtilsProvider>
    );
  }
}

export default compose(
  withStyles(materialStyles),
)(LaunchSetBudgetForm);
