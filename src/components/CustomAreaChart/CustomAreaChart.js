// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';
import {
  VictoryChart,
  VictoryStack,
  VictoryArea,
  VictoryAxis,
} from 'victory';
import {
  compose,
} from 'recompose';
import {
  Paper,
  withStyles,
} from '@material-ui/core';

// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './CustomAreaChart.css';

// --------------- CONSTANTS --------------- //

const materialStyles = {
  chartWrapper: {
    height: '100%',
    width : '100%',
  },
  chart: {
    margin: 20,
    height: '100%',
  },
};

const chartStyles = {
  axis: {
    axis      : {
      stroke: "#606060",
    },
    axisLabel : {
      fontSize  : 30,
      padding   : 30,
      fill      : "#606060",
      fontFamily: 'Open Sans !important',
    },
    ticks     : {
      stroke: "#606060",
      size  : 5,
    },
    tickLabels: {
      fontSize  : 30,
      padding   : 0,
      fill      : "#606060",
      fontFamily: 'Open Sans',
    },
  },
};

// --------------- MAIN --------------- //

class CustomAreaChart extends Component {
  render() {
    const {
      width,    // {number}        -> Parent container width.
      height,   // {number}        -> Parent container height.
      data,     // {Array<Object>} -> Chart data.
      classes,
    } = this.props;

    return (
      <Paper className={classes.chartWrapper} elevation={2}>
        <VictoryChart className={classes.chart} width={ width } height={ height } animate={{ duration: 200 }}>
          <VictoryStack colorScale='blue'>
            {data === null ? null : data.map((dataPoint, i) => {
              return (
                <VictoryArea key={ i } data={ dataPoint } interpolation='natural' />
              );
            })}
          </VictoryStack>
          <VictoryAxis label='Date' style={chartStyles.axis} />
          <VictoryAxis dependentAxis label='%' style={chartStyles.axis} />
        </VictoryChart>
      </Paper>
    );
  }
}

export default compose(
  withStyles(materialStyles),
)(CustomAreaChart);
