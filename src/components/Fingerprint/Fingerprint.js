// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';

// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './Fingerprint.css';

// --------------- CONSTANTS --------------- //

const INIT_STATE = {
};

// --------------- MAIN --------------- //

class Fingerprint extends Component {
  constructor(props) {
    super(props);

    this.state = INIT_STATE;
  }

  render() {
    return (
      <div className='fingerprint-root'>
      </div>
    );
  }
}

export default Fingerprint;
