// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';
import {
  Paper,
  TextField,
  withStyles,
} from '@material-ui/core';

// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './LaunchCreateForm.css';

// Constants.

import { FORM_TYPE_ENUM } from '../Pages/Launch';

// --------------- CONSTANTS --------------- //

const materialStyles = {
  root: {
    width       : '100%',
    padding     : 20,
    marginBottom: 20,
  },
  formHeader: {
    marginTop : '5px !important',
    fontWeight: 300,
  },
  formContainer: {
    display       : 'flex',
    flexFlow      : 'column nowrap',
    justifyContent: 'center',
    alignItems    : 'center',
    padding       : '0 40px',
  },
  textField: {
    '& input': {
      fontSize: 14,
    },
  },
};

const INPUT_VARIANT = 'standard';    // Material UI text input variant.

// --------------- MAIN --------------- //

class LaunchCreateForm extends Component {
  render() {
    const {
      classes,
      onFormChange,
      campaignNameInput,
      campaignTargetWebsiteURLInput,
    } = this.props
    return (
      <Paper className={classes.root} elevation={2}>
        <h3 className={classes.formHeader}>Create</h3>
        <form className={classes.formContainer}>
          <TextField fullWidth required
            id          = 'campaign-name-input'
            label       = 'Campaign name'
            placeholder = 'Set your campaign name...'
            className   = {classes.textField}
            value       = {campaignNameInput}
            onChange    = {(evt) => { onFormChange(FORM_TYPE_ENUM.CREATE, 'campaignNameInput', evt.target.value); }}
            margin      = "normal"
            variant     = {INPUT_VARIANT} />
          <TextField fullWidth required
            id          = 'campaign-website-url-input'
            label       = 'Target website URL'
            placeholder = 'Enter the website URL you would like your users to be sent to...'
            className   = {classes.textField}
            value       = {campaignTargetWebsiteURLInput}
            onChange    = {(evt) => { onFormChange(FORM_TYPE_ENUM.CREATE, 'campaignTargetWebsiteURLInput', evt.target.value); }}
            margin      = "normal"
            type        = 'url'
            variant     = {INPUT_VARIANT} />
        </form>
      </Paper>
    );
  }
}

export default withStyles(materialStyles)(LaunchCreateForm);
