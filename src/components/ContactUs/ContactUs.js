// --------------- DEPENDENCIES --------------- //

import React, { Component } from 'react';
import {
  withStyles,
  Paper,
  TextField,
  Button,
} from '@material-ui/core';

// --------------- LOCAL DEPENDENCIES --------------- //

// CSS.

import './ContactUs.css';

// --------------- CONSTANTS --------------- //

const INIT_STATE = {
  nameInput   : '',   // {String} -> User's name input value.
  emailInput  : '',   // {String} -> User's email input value.
  messageInput: '',   // {String} -> User's message input.
};

const materialStyles = theme =>  ({
  root: {
    padding: '30px 150px',
  },
  paperRoot: {
    display       : 'flex',
    flexFlow      : 'column nowrap',
    justifyContent: 'center',
    alignItems    : 'center',
  },
  formInput: {
    marginTop: 0,
    '& > div > div': {
      fontSize: '14px !important',
    },
    '& > div > input': {
      fontSize: '14px !important',
    },
    '& > label': {
      fontSize: '9px !important',
    },
  },
  contactHeader: {
    marginTop       : 0,
    padding         : '15px 0',
    borderRadius    : '4px 4px 0 0',
    backgroundColor : '#373a47',
    color           : '#FFFFFF',
    width           : '100%',
    textAlign       : 'center',
  },
  formRoot: {
    display       : 'flex',
    flexFlow      : 'column nowrap',
    justifyContent: 'center',
    alignItems    : 'center',
    paddingBottom : 20,
    width         : '70%',
  },
  submitButton: {
    backgroundColor: '#1976d2',
    color          : '#FFFFFF',
    fontSize       : 12,
    marginTop      : 20,
  },
});

// --------------- MAIN --------------- //

class ContactUs extends Component {
  constructor(props) {
    super(props);

    this.state = INIT_STATE;
  }

  /**
   * Handle's contact form submit.
   */
  handleSubmit = (evt) => {
    // TODO: Implement.
    evt.preventDefault();
  }

  render() {
    const { classes, } = this.props;
    const {
      nameInput,
      emailInput,
      messageInput,
    } = this.state;

    return (
      <div className={classes.root}>
        <Paper className={classes.paperRoot} elevation={2}>
          <h2 className={classes.contactHeader}>Contact Us!</h2>
          <form className={classes.formRoot} noValidate autoComplete='off'
            onSubmit={this.handleSubmit}>
            <TextField fullWidth required
              id        = 'form-input-name'
              label     = 'Name'
              className = {classes.formInput}
              value     = {nameInput}
              onChange  = {(evt) => this.setState({ nameInput: evt.target.value, })}
              margin    = 'normal' />
            <TextField fullWidth required
              id        = 'form-input-email'
              label     = 'Email'
              className = {classes.formInput}
              value     = {emailInput}
              onChange  = {(evt) => this.setState({ emailInput: evt.target.value, })}
              margin    = 'normal' />
            <TextField fullWidth multiline required
              rows      = {5}
              rowsMax   = {10}
              id        = 'form-input-message'
              label     = 'Message'
              className = {classes.formInput}
              value     = {messageInput}
              onChange  = {(evt) => this.setState({ messageInput: evt.target.value, })}
              margin    = 'normal' />
            <Button
              type      = 'submit'
              variant   = 'contained'
              className = {classes.submitButton}>
              Submit
            </Button>
          </form>
        </Paper>
      </div>
    );
  }
}

export default withStyles(materialStyles)(ContactUs);
