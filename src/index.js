// ---------- DEPENDENCIES ---------- //

import React from 'react';
import ReactDOM from 'react-dom';
import {
  Provider,
} from 'react-redux';

// ---------- LOCAL DEPENDENCIES ---------- //

// CSS.

import './Global.css';

// Pages.

import App from './components/Pages/App';

// Components.

import store from './store';
import * as serviceWorker from './serviceWorker';

// ---------- MAIN ---------- //

ReactDOM.render((
  <Provider store={ store }>
    <App />
  </Provider>
  ), document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
