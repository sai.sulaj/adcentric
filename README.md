# Adcentric

Adcentric takes the challenge of finding your perfect target audience away from you and make it
our problem. We put the advertising horsepower of Fortune 500 companies in the
hands of small businesses.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

The only prerequisite is Node JS.

```
// Mac. 
brew install node
```

### Installing

Install dependencies

```
npm i --save
```

run local development server

```
npm start
```

Server will be running on localhost:3000

## Running the tests *TODO*

**Important:** dotenv only imports environment variables from '.env', not '.env.*'. 

```
npm test
```

### Test Suites

* App
	1. Tests if App component renders.
	2. Tests if child Navigation component renders.
	3. Tests if 6 child Route components render.

## Deployment *TODO*

Add additional notes about how to deploy this on a live system

## Built With

* [React JS](https://reactjs.org/) - Web framework used.
* [Firebase](https://firebase.google.com/) - Database & Authentication.
* [React-Bootstrap](https://react-bootstrap.github.io/) - UI library.
* [React-Burger-Menu](https://github.com/negomi/react-burger-menu) - Sidebar library.
* [React-Loading](https://github.com/fakiolinho/react-loading) - Loading animations library.
* [Redux](https://github.com/reduxjs/react-redux) - Global state management.

## Versioning

[Gitlab](https://gitlab.com/) is used for versioning.

## Authors

* **Saimir Sulaj** - [Gitlab](https://gitlab.com/sai.sulaj)

## Acknowledgments

* Coffee.
